﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "WorldSpace/ParticleCurve"
{
	Properties
	{
		_TintColor("Tint Color", Color) = (0.5,0.5,0.5,0.5)
		_MainTex("Particle Texture", 2D) = "white" {}
		_InvFade("Soft Particles Factor", Range(0.01,3.0)) = 1.0
		_BWEffectOn("B&W Effect On", Range(0, 1)) = 0
	}

	Category
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Blend SrcAlpha One
		AlphaTest Greater .01
		ColorMask RGB
		Cull Off Lighting Off ZWrite Off Fog{ Color(0,0,0,0) 
	}

	SubShader
	{
		Pass
		{

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma multi_compile_particles

		//Global properties set by bend controller
		uniform half3 _CurveOrigin;
		uniform fixed3 _ReferenceDirection;
		uniform half _Curvature;
		uniform fixed3 _Scale;
		uniform half _FlatMargin;
		uniform half _HorizonWaveFrequency;

		float4x4 _Camera2World;
		float4x4 _World2Camera;


		#include "UnityCG.cginc"

		sampler2D _MainTex;
		fixed4 _TintColor;
		uniform fixed _BWEffectOn;

		struct appdata_t 
		{
			float4 vertex : POSITION;
			fixed4 color : COLOR;
			float2 texcoord : TEXCOORD0;
		};

		struct v2f 
		{
			float4 vertex : SV_POSITION;
			fixed4 color : COLOR;
			float2 texcoord : TEXCOORD0;
			#ifdef SOFTPARTICLES_ON
				float4 projPos : TEXCOORD1;
			#endif
		};

		float4 ParticleBend(float4 v)
		{
			//get vertex in world space
			half4 wpos = mul(_Camera2World, v);

			//calc distance from curve origin
			half2 xzDist = (wpos.xz - _CurveOrigin.xz) / _Scale.xz;
			half dist = length(xzDist);

			//get a non 0 length vector
			half2 direction = lerp(_ReferenceDirection.xz, xzDist, min(dist, 1));

			//radial direction relative to reference direction (angle between vert direction and ref direction)
			half theta = acos(clamp(dot(normalize(direction), _ReferenceDirection.xz), -1, 1));

			//adjust distance by flat space
			dist = max(0, dist - _FlatMargin);

			//bend world in parabolic way
			wpos.y -= dist * dist *_Curvature * cos(theta * _HorizonWaveFrequency);

			//convert world pos back into object space
			wpos.xyz = mul(_World2Camera, wpos).xyz;

			return wpos;
		}

		float4 _MainTex_ST;

		v2f vert(appdata_t v)
		{
			v2f o;

			//bend particle
			half4 vpos = ParticleBend(v.vertex);
			v.vertex = vpos;

			o.vertex = UnityObjectToClipPos(v.vertex);
			#ifdef SOFTPARTICLES_ON
				o.projPos = ComputeScreenPos(o.vertex);
				COMPUTE_EYEDEPTH(o.projPos.z);
			#endif
			o.color = v.color;
			o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);



			return o;
		}

		sampler2D_float _CameraDepthTexture;
		float _InvFade;

		fixed4 frag(v2f i) : SV_Target
		{
		#ifdef SOFTPARTICLES_ON
			float sceneZ = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
			float partZ = i.projPos.z;
			float fade = saturate(_InvFade * (sceneZ - partZ));
			i.color.a *= fade;
		#endif

		fixed4 finalCol = 2.0f * i.color * _TintColor * tex2D(_MainTex, i.texcoord);
		fixed lum = Luminance(finalCol.xyz);

		return _BWEffectOn ? fixed4(lum, lum, lum, 1) : finalCol;
		}

		ENDCG
		}

		}
	}
}