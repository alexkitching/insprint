﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "WorldSpace/WorldCurve" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base(RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		//Global properties set by bend controller
	uniform half3 _CurveOrigin;
	uniform fixed3 _ReferenceDirection;
	uniform half _Curvature;
	uniform fixed3 _Scale;
	uniform half _FlatMargin;
	uniform half _HorizonWaveFrequency;

	float4x4 _Camera2World;
	float4x4 _World2Camera;


	// Per material properties
	sampler2D _MainTex;
	fixed4 _Color;
	half _Glossiness;
	half _Metallic;


	struct Input
	{
		float2 uv_MainTex;
	};


	half4 Bend(half4 v)
	{
		//get vertex in world space
		half4 wpos = mul(unity_ObjectToWorld, v);

		//calc distance from curve origin
		half2 xzDist = (wpos.xz - _CurveOrigin.xz) / _Scale.xz;
		half dist = length(xzDist);

		//get a non 0 length vector
		half2 direction = lerp(_ReferenceDirection.xz, xzDist, min(dist, 1));

		//radial direction relative to reference direction (angle between vert direction and ref direction)
		half theta = acos(clamp(dot(normalize(direction), _ReferenceDirection.xz), -1, 1));

		//adjust distance by flat space
		dist = max(0, dist - _FlatMargin);

		//bend world in parabolic way
		wpos.y -= dist * dist * _Curvature * cos(theta * _HorizonWaveFrequency);

		//convert world pos back into object space
		wpos = mul(unity_WorldToObject, wpos);

		//return vert
		return wpos;
	}


	void vert(inout appdata_full v)
	{
		half4 vpos = Bend(v.vertex);

		v.vertex = vpos;
	}

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = c.a;
	}

		ENDCG
	}
	FallBack "Diffuse"
}
