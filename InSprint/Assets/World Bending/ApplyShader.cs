﻿//Author: Alec Brown

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyShader : MonoBehaviour {

  //  public Camera cam;

    private void OnPreCull()
    {
        Shader.SetGlobalMatrix("_Camera2World", this.GetComponent<Camera>().cameraToWorldMatrix);
        Shader.SetGlobalMatrix("_World2Camera", this.GetComponent<Camera>().worldToCameraMatrix);
    }

}
