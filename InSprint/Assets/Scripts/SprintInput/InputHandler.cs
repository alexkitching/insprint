﻿// Author: Alex Kitching
// Class Responsible in Game Car Input 

using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.SprintInput
{
    public sealed class InputHandler : MonoBehaviour
    {
        public float Horizontal { get; private set; }
        // Gyro Variables
        public Vector3 GyroRotationRate { get; private set; }
        public Vector3 GyroGravity { get; private set; }
        public Vector3 GyroAttitude { get; private set; }
        public Vector3 Acceleration { get; private set; }

        [SerializeField]
        private float gyroRotationHitThreshold = 0f;
        public float GyroRotationHitThreshold
        {
            get { return gyroRotationHitThreshold; }
        }

        private delegate void GetInput();
        private GetInput _getInput = null;

        private void Awake()
        {
            if (SystemInfo.supportsGyroscope) // Gyro Present
            {
                // Enable Gyro
                Input.gyro.enabled = true;
                // Assign input delegate to mobile input
                _getInput = GetMobileInput;
            }
            else
            {
                // Assign input delegate to standard horizontal input
                _getInput = GetStandardInput;
            }
        }

        private void Start()
        {
            if (GameController.Instance != null) // Setup Debug Window
                GameController.Instance.debugWindow.Setup(this);
        }

        // Update input every fixed update
        private void FixedUpdate()
        {
            _getInput();
        }

        private void GetStandardInput() // Gets Horizontal Input
        {
            Horizontal = Input.GetAxisRaw("Horizontal");
        }

        private void GetMobileInput() // Gets Mobile Gyro Input
        {
            GyroRotationRate = Input.gyro.rotationRate;
            GyroGravity = Input.gyro.gravity;
            GyroAttitude = Input.gyro.attitude.eulerAngles;
            Acceleration = Input.gyro.userAcceleration;

            if(GyroGravity.x > gyroRotationHitThreshold) // If Gyro gravity is above hit threshold, register hit
            {
                Horizontal = 1;
            }
            else if(GyroGravity.x < -gyroRotationHitThreshold) // If Gyro gravity is below negative hit threshold, register hit
            {
                Horizontal = -1;
            }
            else
            {
                Horizontal = 0;
            }
        }
    }
}
