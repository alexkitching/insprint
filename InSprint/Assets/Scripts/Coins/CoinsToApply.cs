﻿// Author: Pawel Granda
using Assets.Scripts.DatabaseManagement;
using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Coins
{
	[RequireComponent(typeof(BoxCollider))]
	public class CoinsToApply : NetworkBehaviour
	{
		private LevelController levelController;

		void Start()
		{
			levelController = GameController.Instance.LevelController;
		}

		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.name == levelController.LocalPlayer.name)
			{
				CoinsManager.UpdatePlayerCoins(GameController.Instance.LevelController.LocalPlayer.Coins + 1);
			}

			Destroy(gameObject);
		}
	}
}
