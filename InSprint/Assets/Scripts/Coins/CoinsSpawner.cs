﻿// Author: Pawel Granda
// Script that is spawning coins
using System.Collections;
using Assets.Scripts.Managers;
using Assets.Scripts.PowerUps;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Coins
{
    public class CoinsSpawner : NetworkBehaviour
    {
        [SerializeField]
        private Transform spawnPointLeft;

        [SerializeField]
        private Transform spawnPointCenter;

        [SerializeField]
        private Transform spawnPointRight;

        [SerializeField]
        private GameObject coinPrefab;

        public static bool CanSpawn { get; private set; }

        private LevelController levelController;

        private void Start()
        {
            levelController = GameController.Instance.LevelController;
            StartCoroutine(WaitForFewSeconds());
        }

        private void Update()
        {
            if (!isServer || levelController.Opponent == null || !CanSpawn || PowerUpSpawner.CanSpawn)
                return;

            SpawnCoins();
        }

        private void SpawnCoins()
        {
            CanSpawn = false;

            Transform spawnPoint = null;

            // Choose random coin alignment pattern
            int coinPatternRandomizer = Random.Range(0, 5);

            switch (coinPatternRandomizer)
            {
                case 0:
                    //Single horizontal line of 3 coins
                    for (int i = 0; i < 3; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                //SpawnPointLeft           
                                spawnPoint = spawnPointLeft;
                                break;
                            case 1:
                                //SpawnPointCenter
                                spawnPoint = spawnPointCenter;
                                break;
                            case 2:
                                //SpawnPointRight
                                spawnPoint = spawnPointRight;
                                break;
                            default:
                                break;
                        }

                        RpcSpawnPowerup(spawnPoint.position);
                    }
                    break;

                case 1:
                    //Single vertical line of 6 coins on the LEFT lane
                    for (int i = 1; i < 7; i++)
                    {
                        spawnPoint = spawnPointLeft;

                        Vector3 spawnPointPositionOffset = spawnPoint.position;

                        spawnPointPositionOffset.z += i + 3;

                        RpcSpawnPowerup(spawnPointPositionOffset);
                    }
                    break;

                case 2:
                    //Single vertical line of 6 coins on the CENTER lane
                    for (int i = 1; i < 7; i++)
                    {
                        spawnPoint = spawnPointCenter;

                        Vector3 spawnPointPositionOffset = spawnPoint.position;

                        spawnPointPositionOffset.z += i + 6;

                        RpcSpawnPowerup(spawnPointPositionOffset);
                    }
                    break;

                case 3:
                    //Single vertical line of 6 coins on the RIGHT lane
                    for (int i = 1; i < 7; i++)
                    {
                        spawnPoint = spawnPointRight;

                        Vector3 spawnPointPositionOffset = spawnPoint.position;

                        spawnPointPositionOffset.z += i + 6;

                        RpcSpawnPowerup(spawnPointPositionOffset);
                    }
                    break;

                case 4:
                    //Six horizontal lines of 1 coin each lane
                    for (int i = 0; i < 3; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                //SpawnPointLeft           
                                spawnPoint = spawnPointLeft;
                                break;
                            case 1:
                                //SpawnPointCenter
                                spawnPoint = spawnPointCenter;
                                break;
                            case 2:
                                //SpawnPointRight
                                spawnPoint = spawnPointRight;
                                break;
                            default:
                                break;
                        }

                        for (int j = 1; j < 7; j++)
                        {
                            Vector3 spawnPointPositionOffset = spawnPoint.position;

                            spawnPointPositionOffset.z += j + 6;

                            RpcSpawnPowerup(spawnPointPositionOffset);
                        }
                        
                    }
                    break;

                default:
                    break;
            }

            StartCoroutine(WaitForFewSeconds());
        }

        [ClientRpc]
        private void RpcSpawnPowerup(Vector3 spawnPosition)
        {
            Vector3 spawnPointPosition = spawnPosition;

            if (levelController != null && levelController.Opponent != null)
            {
                float distanceBetweenPlayers = levelController.LocalPlayer.GetComponent<LocalMovement>().OpponentDistance;
                if (distanceBetweenPlayers > 0)
                {
                    spawnPointPosition += new Vector3(0, 0, distanceBetweenPlayers);
                }
            }

            spawnPointPosition.z += 3;
            Instantiate(coinPrefab, spawnPointPosition, Quaternion.identity);

        }

        IEnumerator WaitForFewSeconds()
        {
            yield return new WaitForSeconds(Random.Range(3, 7));
            CanSpawn = true;
        }
    }
}
