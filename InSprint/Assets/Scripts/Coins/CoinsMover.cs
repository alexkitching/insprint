﻿// Author: Pawel Granda

using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Coins
{
	public class CoinsMover : NetworkBehaviour
	{
		private LevelController levelController;
		private Car localPlayerCar;

		void Start()
		{
			levelController = GameController.Instance.LevelController;
			localPlayerCar = levelController.LocalPlayer.GetComponent<Car>();
		}

		void Update()
		{
			UpdateMovement();
		}

		private void UpdateMovement()
		{
			// Calculate the velocity and position of a coin regarding to the local player car velocity
			float zAxisVelocity = -localPlayerCar.CurrentVelocity * Time.deltaTime;
			transform.position += new Vector3(0f, 0f, zAxisVelocity);

			// Destroy coin if its 10 units behind the car
			if (transform.position.z < -10f)
			{
				Destroy(gameObject);
			}
		}

	}
}
