﻿// Author: Alex Kitching
// Base Class Responsible for Assigning Interactable layers and defining definition of Interact Abstract Function
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.Garage.Interactables
{
    public abstract class InteractablePart : MonoBehaviour
    {
        private void Awake ()
        {
            // Assign Interactable Layer to Transforms
            CarTransUtil.AssignLayerOnTransformAndChild(transform, "Interactable");
        }

        // Base Interact Method called via Click Handler
        public abstract void Interact(CameraInputHandler.FocusCamClbk a_focusClbk);
    }
}
