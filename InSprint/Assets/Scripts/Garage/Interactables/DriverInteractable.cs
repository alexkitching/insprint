﻿// Author: Alex Kitching
// Class responsible for holding Interactable Position/Rotation cam data and defining override for Interact Function
using UnityEngine;

namespace Assets.Scripts.Garage.Interactables
{
    public sealed class DriverInteractable : InteractablePart
    {
        private const float DRIVER_CAM_POS_ZOFFS = -2f;
        private const float DRIVER_CAM_POS_YOFFS = 1f;

        public override void Interact(CameraInputHandler.FocusCamClbk a_focusClbk)
        {
            // Adjust Focus Position
            Vector3 pos = transform.position;
            pos.z += DRIVER_CAM_POS_ZOFFS;
            pos.y += DRIVER_CAM_POS_YOFFS;

            // Adjust focus Rot
            Vector3 targetRot = Vector3.zero;
            targetRot.x = GarageHelper.CAMPIVOT_DEFAULT_X;

            // Start Cam Focus
            a_focusClbk(pos, targetRot);
        }
    }
}
