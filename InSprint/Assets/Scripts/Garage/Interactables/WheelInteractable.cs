﻿// Author: Alex Kitching
// Class responsible for holding Interactable Position/Rotation cam data and defining override for Interact Function
using UnityEngine;

namespace Assets.Scripts.Garage.Interactables
{
    public sealed class WheelInteractable : InteractablePart
    {
        private bool _orientation;

        private const float WHEEL_CAM_POS_XOFFS = 1.5f;
        private const float WHEEL_CAM_POS_YOFFS = 0.5f;

        // Used to Assign Wheel its Orientation
        // Left = false, Right = true
        public void AssignOrientation(bool a_orientation)
        {
            _orientation = a_orientation;
        }

        public override void Interact(CameraInputHandler.FocusCamClbk a_focusClbk)
        {
            // Adjust Focus Position and Rotation
            Vector3 pos = transform.position;
            Vector3 targetRot = Vector3.zero;
            targetRot.x = GarageHelper.CAMPIVOT_DEFAULT_X;

            if(_orientation) // If Right we invert normal
            {
                pos.x += -WHEEL_CAM_POS_XOFFS;
                targetRot.y = 90f;
            }
            else
            {
                pos.x += WHEEL_CAM_POS_XOFFS;
                targetRot.y = 270f;
            }

            pos.y += WHEEL_CAM_POS_YOFFS;

            // Start Cam Focus
            a_focusClbk(pos, targetRot);
        }
    }
}
