﻿// Author: Alex Kitching
// Class responsible for holding Interactable Position/Rotation cam data and defining override for Interact Function
using UnityEngine;

namespace Assets.Scripts.Garage.Interactables
{
    public sealed class ChassisInteractable : InteractablePart
    {
        private const float CHASSIS_CAM_POS_XZOFFS = -2.5f;
        private const float CHASSIS_CAM_POS_YOFFS = 1.25f;
        private const float CHASSIS_CAM_ROT_YANGLE = 45f;

        public override void Interact(CameraInputHandler.FocusCamClbk a_focusClbk)
        {
            // Adjust Focus Position
            Vector3 pos = transform.position;
            pos.y += CHASSIS_CAM_POS_YOFFS;
            pos.z += CHASSIS_CAM_POS_XZOFFS;
            pos.x += CHASSIS_CAM_POS_XZOFFS;

            // Adjust focus Rot
            Vector3 targetRot = Vector3.zero;
            targetRot.x = GarageHelper.CAMPIVOT_DEFAULT_X;
            targetRot.y = CHASSIS_CAM_ROT_YANGLE;

            // Start Cam Focus
            a_focusClbk(pos, targetRot);
        }
    }
}
