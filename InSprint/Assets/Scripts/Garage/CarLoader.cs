﻿// Author: Alex Kitching
// Class Responsible Car Loading within Garage Scene
using Assets.Scripts.CarCustomisation;
using Assets.Scripts.Garage.Interactables;
using Assets.Scripts.Garage.UI;
using Assets.Scripts.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Garage
{
    public sealed class CarLoader : MonoBehaviour
    {
        private CurrentChassis _currentChassis;
        private CurrentDriver _currentDriver;
        private CurrentWheels _currentWheels;

        public CarLoadout CurrentLoadout;

        public CarPartList PartList;

        [SerializeField]
        private InputField _carNameInput;

        private const string WHEEL_PIVOT_TAG = "WheelPivots";
        private const string DRIVER_PIVOT_TAG = "DriverPivot";

        private DatabaseManagement.CarPartsManager _carPartManager;

        [SerializeField]
        private SavedNotificationFader _notificationFader;

        private bool _isChangesSaved = true;
        public bool ChangesSaved
        {
            get { return _isChangesSaved; }
        }

        private void Awake()
        {
            _carPartManager = GetComponent<DatabaseManagement.CarPartsManager>();

            // Assign default car parts as default
            CurrentLoadout = new CarLoadout(0, 0, 0, "");

            _currentChassis = new CurrentChassis(-1, null, null, null, null);
            _currentDriver = new CurrentDriver(-1, null, null);
            _currentWheels = new CurrentWheels(-1, null, null);
        }

        private void Start()
        {
            // Get Current Loadout from Car Parts Manager
            CurrentLoadout = _carPartManager.GetCurrentLoadout();
            // Get Car Name Text and assign to Label
            _carNameInput.text = CurrentLoadout.Name;
            // Begin loading Car
            LoadChassis(CurrentLoadout.ChassisId);
        }

        #region CarPart Instantiation

        // Called when switching parts using Part Switcher, switches part based on type and ID
        public void ChangePart(PartSwitcher.PartType a_type, int a_id)
        {
            switch (a_type)
            {
                case PartSwitcher.PartType.CHASSIS:
                    LoadChassis(a_id);
                    break;
                case PartSwitcher.PartType.DRIVER:
                    LoadDriver(a_id, _currentChassis.DriverPivotTransform);
                    break;
                case PartSwitcher.PartType.WHEEL:
                    LoadWheel(a_id, _currentChassis.WheelPivotTransform);
                    break;
            }
        }

        // Loads Chassis and continues to reload wheels and driver
        private void LoadChassis(int a_id)
        {
            // Assign ID and Chassis
            _currentChassis.ID = a_id;
            _currentChassis.Chassis = PartList.Chassis[a_id];

            // Destroy Object if not null
            if (_currentChassis.Object != null)
                Destroy(_currentChassis.Object);

            // Create Chassis Object
            _currentChassis.Object = Instantiate(_currentChassis.Chassis.Prefab, transform);
            _currentChassis.Object.transform.GetChild(0).gameObject.AddComponent<ChassisInteractable>();

            // Assign Transform Pivots
            foreach (Transform child in _currentChassis.Object.transform)
            {
                switch (child.gameObject.tag)
                {
                    case WHEEL_PIVOT_TAG:
                        _currentChassis.WheelPivotTransform = child;
                        break;
                    case DRIVER_PIVOT_TAG:
                        _currentChassis.DriverPivotTransform = child;
                        break;
                }
            }

            // Assign Default Shader
            CarTransUtil.SetTransformAndChildMaterialsToShader(_currentChassis.Object.transform, Shader.Find("Standard"));

            // Load Wheels
            LoadWheel(CurrentLoadout.WheelId, _currentChassis.WheelPivotTransform);
            // Load Driver
            LoadDriver(CurrentLoadout.DriverId, _currentChassis.DriverPivotTransform);
        }

        // Loads wheels under Wheel Transform Pivot of Chassis
        private void LoadWheel(int a_id, Transform a_pivotTrans)
        {
            // Wheels not previously initialised or Wheel Count is Different
            if (_currentWheels.Objects == null ||
                _currentWheels.Objects.Length != _currentChassis.Chassis.WheelCount) 
            {
                // Initialise Wheels
                _currentWheels.Objects = new GameObject[_currentChassis.Chassis.WheelCount];
            }

            // Clear Existing wheels
            foreach (GameObject wheel in _currentWheels.Objects)
            {
                Destroy(wheel);
            }

            _currentWheels.ID = a_id;
            _currentWheels.Wheel = PartList.Wheels[a_id];

            // Instantiate each wheel 
            for (int i = 0; i < _currentWheels.Objects.Length; ++i)
            {
                _currentWheels.Objects[i] = Instantiate(_currentWheels.Wheel.Prefab, a_pivotTrans.GetChild(i));
                WheelInteractable wheelinteract = _currentWheels.Objects[i].AddComponent<WheelInteractable>();

                // Rotate based on Orientation
                if (a_pivotTrans.GetChild(i).name.Contains("Left"))
                {
                    wheelinteract.AssignOrientation(false);
                    _currentWheels.Objects[i].transform.Rotate(0f, 180f, 0f);
                }
                else if (a_pivotTrans.GetChild(i).name.Contains("Right"))
                {
                    wheelinteract.AssignOrientation(true);
                }
            }

            // Assign Standard Shader
            CarTransUtil.SetTransformAndChildMaterialsToShader(a_pivotTrans, Shader.Find("Standard"));
        }

        // Loads Driver under Driver Transform Pivot of Chassis
        private void LoadDriver(int a_id, Transform a_pivotTrans)
        {
            if (_currentDriver.Object != null) // if driver already exists, destroy
            {
                Destroy(_currentDriver.Object);
            }

            _currentDriver.ID = a_id;
            _currentDriver.Driver = PartList.Drivers[a_id];

            // Instantiate Driver 
            _currentDriver.Object = Instantiate(_currentDriver.Driver.Prefab, a_pivotTrans);
            _currentDriver.Object.AddComponent<DriverInteractable>();

            // Assign Standard Shader
            CarTransUtil.SetTransformAndChildMaterialsToShader(a_pivotTrans, Shader.Find("Standard"));
        }

        #endregion

        #region Part Saving

        // Saves Current Selected Part of Type as Part of the Current Loadout
        public void SavePart(PartSwitcher.PartType a_type)
        {
            if (a_type == PartSwitcher.PartType.CHASSIS)
            {
                CurrentLoadout.ChassisId = _currentChassis.ID;
            }
            else if (a_type == PartSwitcher.PartType.DRIVER)
            {
                CurrentLoadout.DriverId = _currentDriver.ID;
            }
            else if (a_type == PartSwitcher.PartType.WHEEL)
            {
                CurrentLoadout.WheelId = _currentWheels.ID;
            }

            _isChangesSaved = false;
        }

        // Saves Current Loadout to Database
        public void SaveLoadout()
        {
            CurrentLoadout.Name = _carNameInput.text;
            _carPartManager.SaveLoadout(CurrentLoadout);
            _notificationFader.ShowNotification();

            _isChangesSaved = true;
        }

        #endregion

        #region Part Resetting

        // Resets Part to Current Loadout
        public void ResetPart(PartSwitcher.PartType a_type)
        {
            switch (a_type)
            {
                case PartSwitcher.PartType.CHASSIS:
                    LoadChassis(CurrentLoadout.ChassisId);
                    break;
                case PartSwitcher.PartType.DRIVER:
                    LoadDriver(CurrentLoadout.DriverId, _currentChassis.DriverPivotTransform);
                    break;
                case PartSwitcher.PartType.WHEEL:
                    LoadWheel(CurrentLoadout.WheelId, _currentChassis.WheelPivotTransform);
                    break;
            }
        }

        #endregion

        #region CurrentPart Structs

        // Structs Responsible for Holding Current Part Data used when Loading in Car 
        private struct CurrentChassis
        {
            public int ID;
            public Chassis Chassis;
            public GameObject Object;
            public Transform WheelPivotTransform;
            public Transform DriverPivotTransform;

            public CurrentChassis(int a_id, Chassis a_chassis, GameObject a_object, Transform a_wheelPivot, Transform a_driverPivot)
            {
                ID = a_id;
                Chassis = a_chassis;
                Object = a_object;
                WheelPivotTransform = a_wheelPivot;
                DriverPivotTransform = a_driverPivot;
            }
        }

        private struct CurrentDriver
        {
            public int ID;
            public Driver Driver;
            public GameObject Object;

            public CurrentDriver(int a_id, Driver a_driver, GameObject a_object)
            {
                ID = a_id;
                Driver = a_driver;
                Object = a_object;
            }
        }

        private struct CurrentWheels
        {
            public int ID;
            public Wheel Wheel;
            public GameObject[] Objects;

            public CurrentWheels(int a_id, Wheel a_wheel, GameObject[] a_objects)
            {
                ID = a_id;
                Wheel = a_wheel;
                Objects = a_objects;
            }
        }

        #endregion
    }
}
