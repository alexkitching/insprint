﻿// Author: Alex Kitching
// Class Responsible Camera Control with in Garage Scene
using System;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.Garage
{
    public sealed class CameraInputHandler : MonoBehaviour
    {
        // Rotation
        [Header("Rotation")]
        [SerializeField]
        private Transform _camPivot;
        [SerializeField]
        private float _rotAcceleration;
        [SerializeField]
        private float _drag;
        [SerializeField]
        private float _xRotationBound;
        [SerializeField]
        private float _maxRotRate;

        private Vector3 _verticalEuler;
        private Vector3 _horizontalEuler;
        private Vector2 _rotationVelocity;

        // Focus
        private Vector3 _focusTarget;
        private Quaternion _focusRot;
        public Vector3 PreFocusPos;

        [SerializeField]
        private float _focusMoveSpeed;
        [SerializeField]
        private float _focusRotateSpeed;

        private bool _focusRotComplete;
        public bool FocusInProgress { get; private set; }

        public delegate void FocusCamClbk(Vector3 a_position, Vector3 a_camEuler);

        private delegate void RotationHandler();
        private RotationHandler _rotationHandler;

        // Touch Input
        private Vector2 _touchStartPos;
        private Vector2 _touchNewPos;

        private void Awake()
        {
            // Starting mode is Free Rotation
            _rotationHandler = FreeRotation;
            _focusRotComplete = false;
            FocusInProgress = false;
        }

        private void Update ()
        {
            if (GarageHelper.Instance.ExitPressed)
                return;

            if(_rotationHandler != null) // Rotation mode assigned
            {
                _rotationHandler(); // Execute Rotation Mode
            }
            else // No Rotation mode assigned (Must be focused)
            {
                if(Input.GetKeyDown("space"))
                {
                    GarageHelper.Instance.DeFocus();
                }
            }
        }

        #region Rotation Functions

        // Allows for free camera rotation around pivot using touch input or key input
        private void FreeRotation()
        {
            bool isMoving = false;
            sbyte iHorizontal = 0;
            sbyte iVertical = 0;

            // Get Input
            GetRotationInput(ref iHorizontal, ref iVertical);

            if (iHorizontal != 0) // Apply Horizontal Acceleration
            {
                ApplyHorizontalAcceleration(iHorizontal);
                isMoving = true;
            }

            if (iVertical != 0) // Apply Vertical Acceleration
            {
                ApplyVerticalAcceleration(iVertical);
                isMoving = true;
            }

            if (!isMoving) // Not moving, apply drag
            {
                if(Math.Abs(_rotationVelocity.y) > float.Epsilon)
                {
                    ApplyDrag(ref _rotationVelocity.y);
                }

                if (Mathf.Abs(_rotationVelocity.x) > float.Epsilon)
                {
                    ApplyDrag(ref _rotationVelocity.x);
                }
            }

            // If Rotation Velocity is present apply rotation and clamp
            if (_rotationVelocity.y > 0 || _rotationVelocity.y < 0)
            {
                if (Mathf.Abs(_rotationVelocity.y) > _maxRotRate)
                    _rotationVelocity = Vector2.ClampMagnitude(_rotationVelocity, _maxRotRate);
                RotateHorizontal(ref _rotationVelocity.y);
            }

            if (_rotationVelocity.x > 0 || _rotationVelocity.x < 0)
            {
                if (Mathf.Abs(_rotationVelocity.x) > _maxRotRate)
                    _rotationVelocity = Vector2.ClampMagnitude(_rotationVelocity, _maxRotRate);
                RotateVertical(ref _rotationVelocity.x);
            }
        }

        // Applies Horizontal Acceleration in Given Direction
        private void ApplyHorizontalAcceleration(sbyte a_dir)
        {
            _rotationVelocity.y += (-a_dir * _rotAcceleration);
        }

        // Applies Vertical Acceleration in Given Direction
        private void ApplyVerticalAcceleration(sbyte a_dir)
        {
            _rotationVelocity.x += (a_dir * _rotAcceleration);
        }

        // Rotates along horizontal axis by given speed
        private void RotateHorizontal(ref float a_speed)
        {
            float yRot = _camPivot.rotation.eulerAngles.y + a_speed * Time.deltaTime;
            _horizontalEuler.Set(_camPivot.rotation.eulerAngles.x, yRot, 0f);
            _camPivot.rotation = Quaternion.Euler(_horizontalEuler);
        }

        // Rotates along Vertical axis by given speed (With Clamping)
        private void RotateVertical(ref float a_speed)
        {
            float xRot = _camPivot.rotation.eulerAngles.x + a_speed * Time.deltaTime;
            if (a_speed > 0)
            {
                if (xRot > _xRotationBound && xRot < 360f - _xRotationBound)
                {
                    xRot = _xRotationBound;
                    a_speed = 0f;
                } 
            }
            else if (a_speed < 0)
            {
                if (xRot < 360f - _xRotationBound && xRot < 360f && xRot > _xRotationBound)
                {
                    xRot = 360f - _xRotationBound;
                    a_speed = 0f;
                } 
            }

            _verticalEuler.Set(xRot, _camPivot.rotation.eulerAngles.y, 0f);
            _camPivot.rotation = Quaternion.Euler(_verticalEuler);
        }

        #endregion

        #region Focus Functions

        // Rotation Mode which rotates camera towards focused object
        private void LookAtFocus()
        {
            if (!_focusRotComplete) // Focus Rotation Inpcomplete
            {
                if (_camPivot.rotation != _focusRot) // Rotation not equal to Focus Rotation
                {
                    _camPivot.rotation = MathUtil.RotateTowards(_focusRot, _camPivot.rotation, _focusRotateSpeed * Time.deltaTime);
                }
                else // Rotation is equal to focus rotation
                {
                    // Assign Prefocus Position (For Exiting focus)
                    PreFocusPos = transform.position;
                    _focusRotComplete = true;
                }
            }
            else // Focus Rotation Complete
            {
                if (transform.position != _focusTarget) // Position not equal to focus position
                {
                    transform.position = Vector3.MoveTowards(transform.position, _focusTarget, _focusMoveSpeed * Time.deltaTime);
                }
                else // Position is equal to focus position
                {
                    FocusInProgress = false;
                    GarageHelper.Instance.ToggleFocusPanel();
                    _rotationHandler = null;
                }
            }
        }

        // Rotation mode which rotates camera back into free rotation
        private void ReturnToPreFocus()
        {
            if (transform.position != PreFocusPos) // Move Camera to Prefocus Position while incorrect
            {
                transform.position = Vector3.MoveTowards(transform.position, PreFocusPos, _focusMoveSpeed * Time.deltaTime);
            }
            else // Position correct return to free rotation mode
            {
                _rotationHandler = FreeRotation;
            }
        }

        #endregion

        // Applies Drag to given axis
        private void ApplyDrag(ref float a_speed)
        {
            if (Mathf.Abs(a_speed) < _drag * Time.deltaTime)
            {
                a_speed = 0f;
                return;
            }

            if (a_speed > 0)
            {
                a_speed -= _drag * Time.deltaTime;
                return;
            }  

            if (a_speed < 0)
                a_speed += _drag * Time.deltaTime; 
        }

        // Called on Focus Clicked, camera recieves Focus Rotation/Position from Parameters
        public void FocusOnInteractable(Vector3 a_position, Vector3 a_camEuler)
        {
            FocusInProgress = true;
            _rotationHandler = LookAtFocus;
            _focusTarget = a_position;
            _focusRot = Quaternion.Euler(a_camEuler);
        }

        // Called on Cancelling Focus, returns camera back to free rotation
        public void CancelFocus()
        {
            GarageHelper.Instance.ToggleFocusPanel();
            _focusRotComplete = false;
            _rotationHandler = ReturnToPreFocus;
        }

        // Gets Input depending on platform build version
        private void GetRotationInput(ref sbyte a_horizontal, ref sbyte a_vertical)
        {
#if UNITY_ANDROID

        if (Input.touchCount <= 0) return; // No Touches Present

        if (Input.GetTouch(0).phase == TouchPhase.Began) // First Finger has been pressed
        {
            _touchStartPos = Input.GetTouch(0).position; // Get Position
        }

        if (Input.GetTouch(0).phase == TouchPhase.Moved) // First Finger has now moved
        {
    // Get Difference between start position and current position
            Vector2 diff = _touchStartPos - Input.GetTouch(0).position; 

            if (Mathf.Abs(diff.x) > float.Epsilon) // If horizontal Difference is significant
            {
                if (diff.x > 0) // Determine direction
                {
                    a_horizontal = 1;
                }
                else
                {
                    a_horizontal = -1;
                }
            }
            else // Dont move
            {
                a_horizontal = 0;
            }

            if (Mathf.Abs(diff.y) > float.Epsilon) // if vertical difference is significant
            {
                if (diff.y > 0) // Determine direction
                {
                    a_vertical = 1;
                }
                else
                {
                    a_vertical = -1;
                }
            }
            else // Dont move
            {
                a_vertical = 0;
            }
        }
#endif
#if UNITY_EDITOR_WIN
            a_horizontal = (sbyte)Input.GetAxisRaw("Horizontal");
            a_vertical = (sbyte)Input.GetAxisRaw("Vertical");
#endif
        }
    }
}
