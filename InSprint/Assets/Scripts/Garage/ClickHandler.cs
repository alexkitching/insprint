﻿// Author: Alex Kitching
// Class Responsible Click/Touch Input
using Assets.Scripts.Garage.Interactables;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Garage
{
    [RequireComponent(typeof(Camera))]
    [RequireComponent(typeof(CameraInputHandler))]
    public sealed class ClickHandler : MonoBehaviour
    {
        public LayerMask InteractableLayer;

        private Camera _camera;

        private RaycastHit _hitinfo;
        private Ray _ray;

        private void Start ()
        {
            _camera = GetComponent<Camera>();
        }
	
        private void Update ()
        {
            if (!Input.GetMouseButtonDown(0)) // Click Input Recieved
                return;

#if UNITY_EDITOR_WIN
            if (EventSystem.current.IsPointerOverGameObject()) // Check Click isn't over UI object
                return;
#endif

#if UNITY_ANDROID
	    if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) // Check Touch isn't over UI object
	        return;
#endif

            if(GarageHelper.Instance.FocusInProgress() || GarageHelper.Instance.ExitPressed) // Check if Focus 
                return;

            if (GarageHelper.Instance.IsFocused)
            {
                GarageHelper.Instance.DeFocus();
                return;
            }

            _ray = _camera.ScreenPointToRay(Input.mousePosition); // Get Ray from Click Position

            if (Physics.Raycast(_ray, out _hitinfo, float.PositiveInfinity, InteractableLayer)) // Check for Ray Collision on Interactable
            {
                GarageHelper.Instance.Focus(_hitinfo.collider.gameObject.GetComponent<InteractablePart>()); // Start focus on Interactable
            }
        }
    }
}
