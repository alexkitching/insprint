﻿// Author: Alex Kitching
// Struct which holds relevant SwitcherUIItem data for use with the Part Switcher UI.
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Garage.UI
{
    public struct SwitcherUIItem
    {
        public RectTransform Transform;
        public int PositionId;
        public int PartId;

        public RawImage RawImage;
    }
}
