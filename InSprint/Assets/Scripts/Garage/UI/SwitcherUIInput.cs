﻿// Author: Alex Kitching
// Class Responsible Part Switcher UI Input
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Garage.UI
{
    [RequireComponent(typeof(SwitcherUI))]
    public sealed class SwitcherUIInput : MonoBehaviour
    {
        private SwitcherUI _switcherUi;

        [SerializeField]
        private GameObject _selectPartButtonGameObject;

        private Button _selectPartButton;

        private void Awake ()
        {
            _switcherUi = GetComponent<SwitcherUI>();
            _selectPartButton = _selectPartButtonGameObject.GetComponent<Button>();
        }
	
        private void Update ()
        {
            UpdateSelectPartButton();
            if (!_switcherUi.CanSwitch || GarageHelper.Instance.ExitPressed)
                return;

            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                ScrollLeft();
            }
            else if (Input.GetAxisRaw("Horizontal") < 0)
            {
                ScrollRight();
            }

            if (!Input.GetKeyDown(KeyCode.Return)) return;

            if (_switcherUi.SelectedItem == GarageHelper.Instance.PartSwitcher.CurrentPartId) return;

            SelectPart();
        }

        // Scrolls UI left
        public void ScrollLeft()
        {
            if (!_switcherUi.CanSwitch || GarageHelper.Instance.ExitPressed)
                return;

            _switcherUi.StartScroll(-1);
        }

        // Scrolls UI Right
        public void ScrollRight()
        {
            if (!_switcherUi.CanSwitch || GarageHelper.Instance.ExitPressed)
                return;

            _switcherUi.StartScroll(1);
        }

        // Selects part if unlocked, purchases and unlocked part if coins are available
        public void SelectPart()
        {
            if (GarageHelper.Instance.PartLocker.IsPartLocked(_switcherUi.SelectedItem))
            {
                if (!GarageHelper.Instance.PartLocker.CanUnlockPart(_switcherUi.SelectedItem)) return;
                GarageHelper.Instance.PartLocker.UnlockPart(_switcherUi.SelectedItem);
                GarageHelper.Instance.PartSwitcher.ChangePart();
                _switcherUi.UpdateCurrentItemIcon(true);
            }
            else
            {
                GarageHelper.Instance.PartSwitcher.ChangePart();
                _switcherUi.UpdateCurrentItemIcon(false);
            }
        }

        // Updates Select Part Button's active state
        private void UpdateSelectPartButton()
        {
            if (!_switcherUi.CanSwitch) // Scroll in progress
            {
                _selectPartButtonGameObject.SetActive(false);
                UpdateSelectPartText();
            }
            else
            {
                if (_selectPartButtonGameObject.activeSelf) // Is active
                {
                    if (_switcherUi.SelectedItem == GarageHelper.Instance.PartSwitcher.CurrentPartId) // Selected Item is equal to current item
                    {
                        _selectPartButtonGameObject.SetActive(false); 
                    }
                    else // Selected item is not current item
                    {
                        UpdateSelectPartText();
                    }
                }
                else // not active
                {
                    if (_switcherUi.SelectedItem == GarageHelper.Instance.PartSwitcher.CurrentPartId) return;

                    _selectPartButtonGameObject.SetActive(true);
                }
            }
        }

        // Updates SelectPartButton Text based on whether the Part is locked
        private void UpdateSelectPartText()
        {
            if (GarageHelper.Instance.PartLocker.IsPartLocked(_switcherUi.SelectedItem))
            {
                _selectPartButtonGameObject.transform.GetChild(0).GetComponent<Text>().text = "Buy Part";

                _selectPartButton.interactable = GarageHelper.Instance.PartLocker.CanUnlockPart(_switcherUi.SelectedItem);
            }
            else
            {
                _selectPartButtonGameObject.transform.GetChild(0).GetComponent<Text>().text = "Select Part";
            } 
        }

        // Disable Button On Disable
        private void OnDisable()
        {
            _selectPartButtonGameObject.SetActive(false);
            _selectPartButton.interactable = true;
        }
    }
}
