﻿// Author: Alex Kitching
// Struct which holds relevant SwitcherUIItemSize data for changing Items Sizes in the switcher UI.
using UnityEngine;

namespace Assets.Scripts.Garage.UI
{
    struct SwitcherUIItemSize
    {
        public readonly Vector2[] Items;

        public SwitcherUIItemSize(int a_count)
        {
            Items = new Vector2[a_count];
        }

        public SwitcherUIItemSize(Vector2[] a_items)
        {
            Items = a_items;
        }

        public static bool operator ==(SwitcherUIItemSize a_s1, SwitcherUIItemSize a_s2)
        {
            return a_s1.Equals(a_s2);
        }

        public static bool operator !=(SwitcherUIItemSize a_s1, SwitcherUIItemSize a_s2)
        {
            return !a_s1.Equals(a_s2);
        }

        public bool Equals(SwitcherUIItemSize a_other)
        {
            return Equals(Items, a_other.Items);
        }

        public override bool Equals(object a_obj)
        {
            if (ReferenceEquals(null, a_obj)) return false;
            return a_obj is SwitcherUIItemSize && Equals((SwitcherUIItemSize) a_obj);
        }

        public override int GetHashCode()
        {
            return (Items != null ? Items.GetHashCode() : 0);
        }
    }
}
