﻿// Author: Alex Kitching
// Class Responsible for Swapping Part Prefabs when using the Part Switcher UI
using System.Collections.Generic;
using Assets.Scripts.Garage.Interactables;
using UnityEngine;

namespace Assets.Scripts.Garage.UI
{
    public sealed class PartSwitcher
    {
        public enum PartType
        {
            NULL = -1,
            CHASSIS = 0,
            DRIVER = 1,
            WHEEL = 2
        }
        public List<GameObject> Items { get; private set; }

        public int CurrentPartId { get; private set; }
        public PartType Type;

        private CarLoader _loader;

        // Sets up Game Objects in Items List
        public void Start(InteractablePart a_part, CarLoader a_loader)
        {
            _loader = a_loader;

            if (a_part is ChassisInteractable)
            {
                Type = PartType.CHASSIS;
                Items = new List<GameObject>(_loader.PartList.Chassis.Length);
                for (int i = 0; i < Items.Capacity; ++i)
                {
                    Items.Add(_loader.PartList.Chassis[i].Prefab);
                }

                CurrentPartId = a_loader.CurrentLoadout.ChassisId;
            }
            else if (a_part is DriverInteractable)
            {
                Type = PartType.DRIVER;
                Items = new List<GameObject>(_loader.PartList.Drivers.Length);
                for (int i = 0; i < Items.Capacity; ++i)
                {
                    Items.Add(_loader.PartList.Drivers[i].Prefab);
                }

                CurrentPartId = a_loader.CurrentLoadout.DriverId;
            }
            else if (a_part is WheelInteractable)
            {
                Type = PartType.WHEEL;
                Items = new List<GameObject>(_loader.PartList.Wheels.Length);
                for (int i = 0; i < Items.Capacity; ++i)
                {
                    Items.Add(_loader.PartList.Wheels[i].Prefab);
                }

                CurrentPartId = a_loader.CurrentLoadout.WheelId;
            }
        }

        public int PartCount
        {
            get { return Items.Count; }
        }

        // Changes Part to Prefab
        public void PreviewPart(int a_id)
        {
            _loader.ChangePart(Type, a_id);
        }

        // Changes Part to Prefab and Saves Part
        public void ChangePart()
        {
            _loader.SavePart(Type);
            switch (Type)
            {
                case PartType.CHASSIS:
                    CurrentPartId = _loader.CurrentLoadout.ChassisId;
                    break;
                case PartType.DRIVER:
                    CurrentPartId = _loader.CurrentLoadout.DriverId;
                    break;
                case PartType.WHEEL:
                    CurrentPartId = _loader.CurrentLoadout.WheelId;
                    break;
            }

            Debug.Log("Part Saved");
        }

        // Resets part List and prefab
        public void Reset()
        {
            _loader.ResetPart(Type);
            Items.Clear();
            Items.Capacity = 0;
        }
    }
}
