﻿// Author: Alex Kitching
// Struct which holds relevant RenderItem data for use with the Part Switcher Renderer.
using UnityEngine;

namespace Assets.Scripts.Garage.UI
{
    struct RenderItem
    {
        public readonly int ID;
        public readonly GameObject gameObject;

        public RenderItem(int a_id, GameObject a_gameObject)
        {
            ID = a_id;
            gameObject = a_gameObject;
        }
    }
}