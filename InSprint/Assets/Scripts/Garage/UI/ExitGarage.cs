﻿// Author: Alex Kitching
// Class Responsible for Exiting Garage Scene on Button Click

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Garage.UI
{
    public sealed class ExitGarage : MonoBehaviour
    {
        private CarLoader _loader;

        [SerializeField]
        private GameObject _saveWarning;

        [SerializeField]
        private Button[] _uiButtonsToDisable;

        private void Start ()
        {
            if (GarageHelper.Instance != null)
            {
                _loader = GarageHelper.Instance.Loader;
            }
        }

        public void Exit() // Called on Exit Button Pressed
        {
            if (_loader.ChangesSaved)
            {
                ChangeToMainMenu();
            }
            else // Display Save Changes Warning
            {
                GarageHelper.Instance.ExitPressed = true;
                foreach (Button button in _uiButtonsToDisable)
                {
                    button.interactable = false;
                }
                _saveWarning.SetActive(true);
            }
        }

        // Changes Scene to Main Menu
        public void ChangeToMainMenu()
        {
            SceneManager.LoadScene(1);
        }

        // Saves Loadout and Exits to Menu
        public void SaveAndExit()
        {
            _loader.SaveLoadout();
            ChangeToMainMenu();
        }
    }
}
