﻿// Author: Alex Kitching
// Inherited Class Responsible for the fading in and out of the saved notification
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Garage.UI
{
    public sealed class SavedNotificationFader : UIFader
    {
        [SerializeField]
        private float _secondsBeforeFadeOut;

        private bool _isShowing;

        private FadeClbk _fadeCompleteClbk;

        private float _waitTimeLeft;

        // Shows notification if not already showing
        public void ShowNotification()
        {
            if (!_isShowing)
            {
                _isShowing = true;
                _fadeCompleteClbk = StartWait;
                FadeIn(_fadeCompleteClbk);
            }
            else if(_waitTimeLeft > 0)
            {
                // Currently waiting so reset timer
                _waitTimeLeft = _secondsBeforeFadeOut;
            }
        }

        // Starts waiting coroutine to fadeout after wait time
        private void StartWait()
        {
            _fadeCompleteClbk = null;
            _waitTimeLeft = _secondsBeforeFadeOut;
            StartCoroutine(WaitToFadeOut());
        }

        // Fades out Notification after wait time
        private IEnumerator WaitToFadeOut()
        {
            while (_waitTimeLeft > 0)
            {
                _waitTimeLeft -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            _isShowing = false;
            FadeOut(_fadeCompleteClbk);
        }
    }
}
