﻿// Author: Alex Kitching
// Class Responsible Part Switcher UI Functionality
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Garage.UI
{
    public sealed class SwitcherUI : MonoBehaviour
    {
        // Switcher Item
        [SerializeField]
        private GameObject _itemPrefab;
        private float _itemWidth;
        private float _itemHeight;

        // Item Sizes
        private const float SELECTED_ITEM_SIZE_MULTIPLIER = 1.2f;
        private SwitcherUIItemSize _defaultItemSize;
        private SwitcherUIItemSize _selectedItemSize;

        private const int ITEM_SLOT_COUNT = 7;
        public int ItemSlotCount
        {
            get { return ITEM_SLOT_COUNT; }
        }

        public int SelectedItem { get; private set; }

        private Vector2[] _itemSlotPositions;
        private SwitcherUIItem[] _items;
        private SwitcherUIItem _currentSavedItem;

        private bool _shouldScroll;
        public bool CanSwitch = true;

        [SerializeField]
        private float _scrollSpeed;

        private const int IMAGE_CHILD_INDEX = 0;
        private const int TICK_ICON_CHILD_INDEX = 1;
        private const int LOCK_ICON_CHILD_INDEX = 2;
        private const int COIN_TEXT_CHILD_INDEX = 3;
        private const int COIN_ICON_CHILD_INDEX = 4;

        private void Awake()
        {
            // Initialise Arrays
            _items = new SwitcherUIItem[ITEM_SLOT_COUNT];
            _itemSlotPositions = new Vector2[ITEM_SLOT_COUNT];

            // Setup our Item Sizes
            int itemPrefabSizeCount = _itemPrefab.transform.childCount + 1;
            _defaultItemSize = new SwitcherUIItemSize(itemPrefabSizeCount);
            _selectedItemSize = new SwitcherUIItemSize(itemPrefabSizeCount);

            // Setup Default/Selected Sizes
            _defaultItemSize.Items[0] = _itemPrefab.GetComponent<RectTransform>().sizeDelta;
            _selectedItemSize.Items[0] = _defaultItemSize.Items[0] * SELECTED_ITEM_SIZE_MULTIPLIER;
            for (int i = 0; i < _itemPrefab.transform.childCount; ++i)
            {
                _defaultItemSize.Items[i + 1] = _itemPrefab.transform.GetChild(i).GetComponent<RectTransform>().sizeDelta;
                _selectedItemSize.Items[i + 1] = _defaultItemSize.Items[i + 1] * SELECTED_ITEM_SIZE_MULTIPLIER;
            }

            _itemWidth = _defaultItemSize.Items[0].x;
            _itemHeight = _defaultItemSize.Items[0].y;

            // Calculate Width and Spacing
            float width = GetComponent<RectTransform>().sizeDelta.x;
            float xSpacing = width / (ITEM_SLOT_COUNT - 2);
            float ySpacing = GetComponent<RectTransform>().sizeDelta.y - _itemHeight;

            // Setup Off Screen Positions
            _itemSlotPositions[0].x = -_itemWidth * 0.7f;
            _itemSlotPositions[0].y = 0.5f * (_itemWidth + ySpacing);
            _itemSlotPositions[6].x = width + (_itemWidth * 0.7f);
            _itemSlotPositions[6].y = 0.5f * (_itemWidth + ySpacing);

            // Setup on Screen Positions
            float offs = xSpacing - 0.75f * _itemWidth;
            for (int i = 1; i < ITEM_SLOT_COUNT - 1; ++i)
            {
                _itemSlotPositions[i].y = 0.5f * (_itemWidth + ySpacing);

                _itemSlotPositions[i].x = offs;
                offs += xSpacing;
            }
        }
 
        private void OnEnable()
        {
            // Assign Selected Item
            SelectedItem = GarageHelper.Instance.PartSwitcher.CurrentPartId;
            // Setup Items
            SetupItemsAndPositions();
            SetSelectedItemSize(_selectedItemSize);

            _currentSavedItem = _items[SelectedItem];
            _currentSavedItem.Transform.GetChild(1).gameObject.SetActive(true);
        }

        private void OnDisable()
        {
            // Clear all Items
            for (int i = 0; i < _items.Length; ++i)
            {
                if (_items[i].Transform == null) continue;
                Destroy(_items[i].Transform.gameObject);
                _items[i].RawImage = null;
                _items[i].PositionId = -1;
                _items[i].PartId = -1;
            }
            // Reset Selected Item
            SelectedItem = 0;
        }

        private void Update ()
        {
            if(_shouldScroll) 
                Scroll();
        }

        private void SetupItemsAndPositions()
        {
            int count = 0;
            int pos = 0;
            // Generate Starting Items
            for (int i = SelectedItem -3; i != SelectedItem + 4; ++i)
            {
                if (i >= GarageHelper.Instance.PartSwitcher.PartCount)
                    break;

                if (i < 0)
                {
                    ++pos;
                    continue;
                }

                LoadItem(count, pos);
                ++pos;
                ++count;
            }
        }

        // Loads Switcher UI ITem
        private void LoadItem(int a_arrayIter, int a_positionID)
        {
            GameObject item = Instantiate(_itemPrefab);
            item.transform.SetParent(transform, false);

            _items[a_arrayIter].Transform = item.GetComponent<RectTransform>();
            _items[a_arrayIter].PositionId = a_positionID;
            _items[a_arrayIter].Transform.anchoredPosition = _itemSlotPositions[a_positionID];
            _items[a_arrayIter].RawImage = item.transform.GetChild(IMAGE_CHILD_INDEX).GetComponent<RawImage>();
            _items[a_arrayIter].RawImage.texture = GarageHelper.Instance.UiRenderer.RenderTextures[a_positionID];

            if (!GarageHelper.Instance.PartLocker.IsPartLocked(a_arrayIter)) return;
            // Part is locked
            item.transform.GetChild(LOCK_ICON_CHILD_INDEX).gameObject.SetActive(true);
            item.transform.GetChild(COIN_TEXT_CHILD_INDEX).gameObject.SetActive(true);
            item.transform.GetChild(COIN_ICON_CHILD_INDEX).gameObject.SetActive(true);

            Text text = item.transform.GetChild(COIN_TEXT_CHILD_INDEX).gameObject.GetComponent<Text>();
        
            if(text)
                text.text = GarageHelper.Instance.PartLocker.GetPartCost(a_arrayIter).ToString();
        }

        // Starts Scrolling Process in given Direction by altering items positions
        public void StartScroll(sbyte a_dir)
        {
            int nextItem = SelectedItem + a_dir;
            if (nextItem < 0)
            {
                nextItem = ITEM_SLOT_COUNT - 1;
            }
            else if (nextItem > ITEM_SLOT_COUNT - 1)
            {
                nextItem = 0;
            }
            if (_items[nextItem].Transform == null)
                return;
            CanSwitch = false;
            SetSelectedItemSize(_defaultItemSize);
            SelectedItem = nextItem;

            for (int i = 0; i < ITEM_SLOT_COUNT; ++i) // Adjust Target Position
            {
                if (_items[i].Transform == null) continue;
                _items[i].PositionId -= a_dir;

                if (_items[i].PositionId < 0)
                {
                    _items[i].PositionId = ITEM_SLOT_COUNT - 1;
                    _items[i].Transform.anchoredPosition = _itemSlotPositions[_items[i].PositionId];
                }
                else if (_items[i].PositionId > ITEM_SLOT_COUNT - 1)
                {
                    _items[i].PositionId = 0;
                    _items[i].Transform.anchoredPosition = _itemSlotPositions[_items[i].PositionId];
                }
            }

            _shouldScroll = true;
        }

        // Scrolls any items which aren't currently in their correct position
        private void Scroll()
        {
            bool scrollIncomplete = false;
            foreach (SwitcherUIItem item in _items)
            {
                if(item.Transform == null) continue;
                if (item.Transform.anchoredPosition == _itemSlotPositions[item.PositionId]) continue;

                item.Transform.anchoredPosition = Vector2.MoveTowards(item.Transform.anchoredPosition, _itemSlotPositions[item.PositionId], _scrollSpeed * Time.deltaTime);
                scrollIncomplete = true;
            }

            if (scrollIncomplete) return;

            GarageHelper.Instance.PartSwitcher.PreviewPart(SelectedItem);
            SetSelectedItemSize(_selectedItemSize);
            _shouldScroll = false;
            CanSwitch = true;
        }

        // Adjusts Selected Items Size
        private void SetSelectedItemSize(SwitcherUIItemSize a_itemSize)
        {
            _items[SelectedItem].Transform.sizeDelta = a_itemSize.Items[0];
            int count = _items[SelectedItem].Transform.childCount;
            for (int i = 0; i < count; ++i)
            {
                _items[SelectedItem].Transform.GetChild(i).GetComponent<RectTransform>().sizeDelta = a_itemSize.Items[i + 1];
            }
        }

        // Updates Current Item based on whether its locked/unlocked
        public void UpdateCurrentItemIcon(bool a_unlocked)
        {
            _currentSavedItem.Transform.GetChild(TICK_ICON_CHILD_INDEX).gameObject.SetActive(false);
            _items[SelectedItem].Transform.GetChild(TICK_ICON_CHILD_INDEX).gameObject.SetActive(true);
            _currentSavedItem = _items[SelectedItem];

            if (!a_unlocked) return;

            _items[SelectedItem].Transform.GetChild(LOCK_ICON_CHILD_INDEX).gameObject.SetActive(false);
            _items[SelectedItem].Transform.GetChild(COIN_TEXT_CHILD_INDEX).gameObject.SetActive(false);
            _items[SelectedItem].Transform.GetChild(COIN_ICON_CHILD_INDEX).gameObject.SetActive(false);
        }
    }
}
