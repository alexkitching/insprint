﻿// Author: Alex Kitching
// Class Responsible UI Item Fading
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Garage.UI
{
    public class UIFader : MonoBehaviour
    {
        public CanvasGroup UiElement;

        public float FadeTime;

        protected delegate void FadeClbk();

        private bool _fadeInProgress = false;
        private bool _fadingIn = false;

        // Default alpha to 0
        private void Awake()
        {
            UiElement.alpha = 0f;
        }

        // Fade in Item 
        protected void FadeIn(FadeClbk a_clbk = null)
        {
            if (_fadeInProgress && !_fadingIn) // If Fadeout In Progress, Stop
                StopCurrentFade();

            if (_fadeInProgress) return;
            _fadingIn = true;
            StartCoroutine(FadeUIGroup(UiElement, UiElement.alpha, 1f, a_clbk, FadeTime));
        }

        // Fade out Item
        protected void FadeOut(FadeClbk a_clbk = null)
        {
            if(_fadeInProgress && _fadingIn) // If FadeIn in progress, stop
                StopCurrentFade();

            if (_fadeInProgress) return;
            _fadingIn = false;
            StartCoroutine(FadeUIGroup(UiElement, UiElement.alpha, 0f, a_clbk, FadeTime));
        }

        // Fades in UI from start alpha to end alpha
        private IEnumerator FadeUIGroup(CanvasGroup cg, float a_start, float a_end, FadeClbk a_completeClbk, float lerpTime = 0.5f)
        {
            _fadeInProgress = true;
            float timeStarted = Time.time;
            float timeSinceStarted = Time.time - timeStarted;

            while (true)
            {
                timeSinceStarted = Time.time - timeStarted;
                float percentageComplete = timeSinceStarted / lerpTime;

                float currentValue = Mathf.Lerp(a_start, a_end, percentageComplete);

                cg.alpha = currentValue;

                if (percentageComplete >= 1)
                    break;

                yield return new WaitForEndOfFrame();
            }

            _fadeInProgress = false;

            if(a_completeClbk != null)
                a_completeClbk();
        }

        // Stops Fade in progress
        private void StopCurrentFade()
        {
            StopCoroutine("FadeUIGroup");
            _fadeInProgress = false;
        }
    }
}
