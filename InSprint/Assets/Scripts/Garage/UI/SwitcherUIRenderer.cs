﻿// Author: Alex Kitching
// Class Responsible Part Switcher UI Image Rendering
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.Garage.UI
{
    public sealed class SwitcherUIRenderer : MonoBehaviour
    {
        private PartSwitcher _switcher;

        [SerializeField]
        private SwitcherUI _switcherUi;

        private Camera _renderCamera;
        private Transform _itemHolder;
        public RenderTexture[] RenderTextures;

        // Part Offsets
        private const float CHASSIS_Z_OFFS = 3f;
        private const float CHASSIS_Y_OFFS = -0.6f;
        private const float CHASSIS_X_ROT = 0f;
        private const float CHASSIS_Y_ROT = -30f;
        private const float DRIVER_Z_OFFS = 0.8f;
        private const float WHEEL_Z_OFFS = 0.6f;
        private const float WHEEL_Y_ROT = -130f;

        private Queue<RenderItem> _itemsToRender;
        private bool _isRendering = false;

        private void Awake()
        {
            _renderCamera = GetComponent<Camera>();
            _renderCamera.enabled = false;

            _itemHolder = transform.GetChild(0);

            // Initialise Render Textures
            RenderTextures = new RenderTexture[_switcherUi.ItemSlotCount];
            for (int i = 0; i < RenderTextures.Length; ++i)
            {
                RenderTextures[i] = new RenderTexture(128, 128, 16, RenderTextureFormat.ARGB32);
            }

            _itemsToRender = new Queue<RenderItem>(_switcherUi.ItemSlotCount);
        }

        // Generates Switch UI Icons
        public void GenInitialIcons(PartSwitcher a_switcher)
        {
            // Free up Render Textures
            for (int i = 0; i < RenderTextures.Length; ++i)
            {
                RenderTextures[i].Release();
            }
            _switcher = a_switcher;

            // Adjust holder based on part type 
            switch (_switcher.Type)
            {
                case PartSwitcher.PartType.CHASSIS:
                    AdjustHolder(CHASSIS_Y_OFFS, CHASSIS_Z_OFFS, CHASSIS_X_ROT, CHASSIS_Y_ROT);
                    break;
                case PartSwitcher.PartType.DRIVER:
                    AdjustHolder(0f, DRIVER_Z_OFFS, 0f, 0f);
                    break;
                case PartSwitcher.PartType.WHEEL:
                    AdjustHolder(0, WHEEL_Z_OFFS, 0f, WHEEL_Y_ROT);
                    break;
            }

            // Get Current Part ID
            int currentId = _switcher.CurrentPartId;
            // Generate Selected Item first
            int pos = 3;
            _itemsToRender.Enqueue(new RenderItem(pos, _switcher.Items[currentId]));

            if (currentId != 0)
            {
                // Generate Previous Items
                pos = 2;
                for (int i = currentId -1; i >= currentId - 3; --i)
                {
                    if (i < 0)
                        break;
                    _itemsToRender.Enqueue(new RenderItem(pos, _switcher.Items[i]));
                    --pos;
                }
            }

            if (currentId + 1 < _switcher.PartCount)
            {
                pos = 4;
                for (int j = currentId + 1; j <= currentId + 3; ++j)
                {
                    _itemsToRender.Enqueue(new RenderItem(pos, _switcher.Items[j]));
                    if (j + 1 == _switcher.PartCount)
                        break;
                    ++pos;
                }
            }

            StartCoroutine(RenderIcon(_itemsToRender.Dequeue()));
        }

        // While rendering is not in progress and there are still items to render, render items in the queue
        private void Update()
        {
            if (!_isRendering &&_itemsToRender.Count > 0)
            {
                StartCoroutine(RenderIcon(_itemsToRender.Dequeue()));
            }
        }

        // Renders Icon to Render Texture from a Render Item
        private IEnumerator RenderIcon(RenderItem a_item)
        {
            _isRendering = true;
            GameObject item = Instantiate(a_item.gameObject, _itemHolder);

            CarTransUtil.SetTransformAndChildMaterialsToShader(item.transform, Shader.Find("Standard"));

            RenderTexture active = RenderTexture.active;
            RenderTexture.active = RenderTextures[a_item.ID];
            // Clear Render Buffer
            GL.Clear(true, true, Color.clear);
            // Clear contents of Render Texture
            RenderTexture.active.DiscardContents();
            RenderTexture.active = active;

            _renderCamera.targetTexture = RenderTextures[a_item.ID];
            _renderCamera.Render();

            // Wait for Frame for Render to Complete
            yield return new WaitForEndOfFrame();
            // Destroy previous item
            Destroy(item);
            // Wait for Frame for Old Item Drawing to clear
            yield return new WaitForEndOfFrame();
            _isRendering = false;
        }

        // Adjusts item holder based on offsets
        private void AdjustHolder(float a_yOffs, float a_zOffs, float a_xRot, float a_yRot)
        {
            _itemHolder.transform.localPosition = new Vector3(0f, a_yOffs, a_zOffs);
            _itemHolder.rotation = Quaternion.Euler(a_xRot, a_yRot, 0f);
        }
    }
}
