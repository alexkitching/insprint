﻿// Author: Alex Kitching
// Class Responsible for Tracking and Updating the Lock Status of Parts
using System;
using Assets.Scripts.DatabaseManagement;
using Assets.Scripts.DatabaseManagement.ObjectModel;
using Assets.Scripts.Garage.Interactables;

namespace Assets.Scripts.Garage.UI
{
    public sealed class PartLocker
    {
        private readonly CarLoader _loader;
        private readonly PartSwitcher _switcher;
        private readonly int _playerId;

        private bool[] _partsIsLocked;
        private int[] _partsCost;

        public PartLocker(CarLoader a_loader, PartSwitcher a_switcher, int a_currentPlayerId)
        {
            _loader = a_loader;
            _switcher = a_switcher;
            _playerId = a_currentPlayerId;
        }

        // Sets up Part Costs from Database
        public void Start(InteractablePart a_part)
        {
            CarPartsRepository partRepo = new CarPartsRepository();

            int count = 0;
            if (a_part is ChassisInteractable)
            {
                count = _loader.PartList.Chassis.Length;

                AssignPartLockedDefaults(count);

                _partsCost = new int[count];

                var parts = partRepo.GetAllChassisPartsOwnedByPlayer(_playerId);

                foreach (ChassisPart part in parts)
                {
                    _partsIsLocked[part.ChassisPartID - 1] = false;
                }

                parts = partRepo.GetAllChassisParts();
                foreach (ChassisPart part in parts)
                {
                    _partsCost[part.ChassisPartID - 1] = part.Cost;
                }
            }
            else if (a_part is DriverInteractable)
            {
                count = _loader.PartList.Drivers.Length;
           
                AssignPartLockedDefaults(count);

                _partsCost = new int[count];

                var parts = partRepo.GetAllBodyPartsOwnedByPlayer(_playerId);

                foreach (BodyPart part in parts)
                {
                    _partsIsLocked[part.BodyPartID - 1] = false;
                    _partsCost[part.BodyPartID - 1] = part.Cost;
                }

                parts = partRepo.GetAllBodyParts();
                foreach (BodyPart part in parts)
                {
                    _partsCost[part.BodyPartID - 1] = part.Cost;
                }
            }
            else if (a_part is WheelInteractable)
            {
                count = _loader.PartList.Wheels.Length;
            
                AssignPartLockedDefaults(count);

                _partsCost = new int[count];

                var parts = partRepo.GetAllWheelPartsOwnedByPlayer(_playerId);

                foreach (WheelPart part in parts)
                {
                    _partsIsLocked[part.WheelPartID - 1] = false;
                    _partsCost[part.WheelPartID - 1] = part.Cost;
                }

                parts = partRepo.GetAllWheelParts();
                foreach (WheelPart part in parts)
                {
                    _partsCost[part.WheelPartID - 1] = part.Cost;
                }
            }
        }

        // Resets Costs
        public void Reset()
        {
            Array.Clear(_partsIsLocked, 0, _partsIsLocked.Length);
            _partsIsLocked = null;
        }

        // Assigns all parts as locked by default
        private void AssignPartLockedDefaults(int a_count)
        {
            _partsIsLocked = new bool[a_count];
            for (int i = 0; i < a_count; ++i)
            {
                _partsIsLocked[i] = true;
            }
        }

        public bool IsPartLocked(int a_id)
        {
            return _partsIsLocked[a_id];
        }

        public int GetPartCost(int a_id)
        {
            return _partsCost[a_id];
        }
        
        // Checks coins to see if part can be unlocked
        public bool CanUnlockPart(int a_id)
        {
            return GarageHelper.Instance.GetCountCount() >= _partsCost[a_id];
        }

        // Unlocks part and updates coins
        public void UnlockPart(int a_id)
        {
            GarageHelper.Instance.RemoveCoins(_partsCost[a_id]);

            PartSwitcher.PartType type = _switcher.Type;

            _partsIsLocked[a_id] = false;

            CarPartsRepository repo = new CarPartsRepository();

            switch (type)
            {
                case PartSwitcher.PartType.CHASSIS:
                    repo.AssignChassisPartToPlayer(_playerId, a_id + 1);
                    break;
                case PartSwitcher.PartType.DRIVER:
                    repo.AssignBodyPartToPlayer(_playerId, a_id + 1);
                    break;
                case PartSwitcher.PartType.WHEEL:
                    repo.AssignWheelPartToPlayer(_playerId, a_id + 1);
                    break;
            }
        }
    }
}
