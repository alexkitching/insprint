﻿// Author: Alex Kitching
// Singleton Class Responsible for Linking Various parts of the Garage System
using Assets.Scripts.DatabaseManagement;
using Assets.Scripts.Garage.Interactables;
using Assets.Scripts.Garage.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Garage
{
    [RequireComponent(typeof(CarLoader))]
    [RequireComponent(typeof(CarPartsManager))]
    public sealed class GarageHelper : MonoBehaviour
    {
        public static GarageHelper Instance;
    
        public CarLoader Loader { get; private set; }
        private CarPartsManager _partManager;
    
        public const float CAMPIVOT_DEFAULT_X = 10f;
    
        // Inputs
        [SerializeField]
        private CameraInputHandler _camInputHandler;
        private CameraInputHandler.FocusCamClbk _focusCamClbk;
    
        // Focus
        public bool IsFocused { get; private set; }
    
        // ExitButton Pressed
        public bool ExitPressed;
    
        // UI
        [SerializeField]
        public SwitcherUIRenderer UiRenderer;
    
        public GameObject Switcher;
        public PartSwitcher PartSwitcher;
        public PartLocker PartLocker;
    
        // Coins
        private int _coinCount;
        [SerializeField]
        private Text _coinText;
    
    	private void Awake ()
        {
    		if(Instance != null) // Garage helper exists
            {
                Destroy(this); // Destroy this
            }
            else // No Previous Exists
            {
                Instance = this;
                Loader = GetComponent<CarLoader>();
                _partManager = GetComponent<CarPartsManager>();
    
                IsFocused = false;
                ExitPressed = false;
                _focusCamClbk = _camInputHandler.FocusOnInteractable;
                PartSwitcher = new PartSwitcher();
            }
            
            // Attempt to get coins from Database
            if (AuthenticationManager.Instance != null)
            {
                CoinsRepository coinRepo = new CoinsRepository();
    
                _coinCount = coinRepo.GetPlayerCoins(AuthenticationManager.Instance.LoggedPlayerName);
                _coinText.text = _coinCount.ToString();
            }
            else // Database is unavailable, default to 0 coins
            {
                _coinCount = 0;
                _coinText.text = "0";
            }
        }
    
        private void Start()
        {
            PartLocker = new PartLocker(Loader, PartSwitcher, _partManager.CurrentPlayerID);
            // Adjust rotation on android
    #if UNITY_ANDROID
            Screen.orientation = ScreenOrientation.LandscapeLeft;
    #endif
        }
        // Focuses on Selected Interactable Part
        public void Focus(InteractablePart a_interactable)
        {
            // Start Part Locker and Switcher
            PartSwitcher.Start(a_interactable, Loader);
            PartLocker.Start(a_interactable);
            // Generate Switcher Item Icons
            UiRenderer.GenInitialIcons(PartSwitcher);
            
            IsFocused = true;

            // Call Interactable interact method
            a_interactable.Interact(_focusCamClbk);
        }
        
        // Defocuses on focused interactable
        public void DeFocus()
        {
            IsFocused = false;
            // Cancel Camera Focus
            _camInputHandler.CancelFocus();
            // Reset switcher and locker
            PartSwitcher.Reset();
            PartLocker.Reset();
        }
    
        public bool FocusInProgress()
        {
            return _camInputHandler.FocusInProgress;
        }
        
        // Toggles focus Panel
        public void ToggleFocusPanel()
        {
            Switcher.SetActive(!Switcher.activeInHierarchy);
        }
    
        public int GetCountCount()
        {
            return _coinCount;
        }
        
        // Removes coins from User and Updates Database
        public void RemoveCoins(int a_amount)
        {
            _coinCount -= a_amount;
            CoinsRepository repo = new CoinsRepository();
            repo.UpdatePlayerCoins(AuthenticationManager.Instance.LoggedPlayerName, _coinCount);
            _coinText.text = _coinCount.ToString();
        }
    
        // Adjust Screen Orientation if on Android
        private void OnDisable()
        {
    #if UNITY_ANDROID
            Screen.orientation = ScreenOrientation.Portrait;
    #endif
        }
    }
}

