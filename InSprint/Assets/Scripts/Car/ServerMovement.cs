﻿//Author: Alec Brown

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class ServerMovement : NetworkBehaviour
{
    //scripts
    private Car car;
    private LocalMovement localMovement;
    private LaneMovement laneMovement;

    //synced vars
    [SyncVar]
    public float playerDistance;
    [SyncVar]
    public float LanexPosition;

    //local vars
    private float P1Pos = 0.0f, P2Pos = 0.0f;

    //keep track of position on server
    public Vector3 ServerPosition;


    private void Start()
    {
        car = GetComponent<Car>();
        localMovement = GetComponent<LocalMovement>();
        laneMovement = GetComponent<LaneMovement>();
    }


    private void Update()
    {
        if (!isServer) return;

        //update lane position on server  (debug purposes - not actually required)
        ServerPosition = new Vector3(laneMovement.CurrentLanexPos, ServerPosition.y, ServerPosition.z);

        //find the offset from the origin
        float positionOriginOffset = 0.0f;

        //calcualte total distance between players
        bool nextPlayer = false;
            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (!nextPlayer)
                {
                    P1Pos = player.GetComponent<ServerMovement>().ServerPosition.z;
                    positionOriginOffset = P1Pos;
                    nextPlayer = true;
                }
                else
                {
                    P2Pos = player.GetComponent<ServerMovement>().ServerPosition.z;
                    if (P2Pos < positionOriginOffset) positionOriginOffset = P2Pos;
                    nextPlayer = false;
                }
            }

            playerDistance = P1Pos - P2Pos;
            Debug.Log("Player distance is: " + playerDistance);


        //loop through players and reset transforms back to origin
            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
            Vector3 pos = player.GetComponent<ServerMovement>().ServerPosition;
                player.GetComponent<ServerMovement>().ServerPosition = new Vector3(pos.x, pos.y, pos.z - positionOriginOffset);
            }

    }


    [Command]
    public void CmdUpdatePosition(int dir)
    {
        //decide the direction of the 'boost'
        float MovementAmount = car.BoostAmt * dir;
        //add a boost to our transform position
        ServerPosition = new Vector3(ServerPosition.x, ServerPosition.y, ServerPosition.z + MovementAmount);

        //update our local velocity
        localMovement.RpcUpdateVelocity(dir);

    }


    //returns true if lateral player movement is not obstructed by the opponent on the server
    //Replaced with local raycasting - faster and less trafic over the network - left this in just in case for the time being
    // also didnt work well with forward/backwards movement
    // to be deleted it not needed later!

    //public bool LaneChangeRequest(GameObject Player, int desiredLanexPos)
    //{
    //    foreach (GameObject Opponent in GameObject.FindGameObjectsWithTag("Player"))
    //    {
    //        //get our opponent
    //        if (Player.GetInstanceID() != Opponent.GetInstanceID())
    //        {
    //            int OpponentLanePos = (int)Opponent.GetComponent<ServerMovement>().LanexPosition;
    //            float playerZpos = Player.GetComponent<ServerMovement>().ServerPosition.z;
    //            float OpponentZpos = Opponent.GetComponent<ServerMovement>().ServerPosition.z;

    //            //check our opponents isnt in the place where we want to be
    //            if (desiredLanexPos != OpponentLanePos || playerZpos != OpponentZpos)
    //            {
    //                return true;
    //            }
    //            break;
    //        }
    //        else
    //        {
    //            int myLanePos = (int)Opponent.GetComponent<ServerMovement>().LanexPosition;
    //        }
    //    }

    //    return false;
    //}
}
