﻿﻿//Author: Alec Brown
using UnityEngine;
using UnityEngine.Networking;
using Assets.Scripts.CarCustomisation;
using Assets.Scripts.Managers;

[RequireComponent(typeof(Player))]
public class PlayerSetup : NetworkBehaviour {

    [SerializeField]
    Behaviour[] componentsToDisable;

    [SerializeField]
    string RemoteLayerName = "RemotePlayer";

    LevelController LC;

    Camera sceneCamera;

    [SyncVar]
    public CarLoadout loadout;

    private void Start()
    {
        CarModelMaker modelMaker = new CarModelMaker(loadout, gameObject);
        modelMaker.BuildModel();

        init();
    }

    public void init()
    {
        //get level controller
        LC = FindObjectOfType<LevelController>();

        if (isLocalPlayer)
        {
            SetUpPlayer();
        }
        else
        {
            SetUpOpponent();
        }

        GetComponent<Player>().SetUp();
    }

    //set up our player
    private void SetUpPlayer()
    {
        sceneCamera = Camera.main;
        if (sceneCamera != null)
        {
            //assign the car script to the local level controller to control road speed
            LC.LocalPlayer = GetComponent<Player>();
            LC.LocalPlayer.ID = int.Parse(LC.LocalPlayer.netId.ToString());

            sceneCamera.gameObject.SetActive(false);
        }
    }

    //set up our opponent
    private void SetUpOpponent()
    {
        //assign remote layer
        gameObject.layer = LayerMask.NameToLayer(RemoteLayerName);

        //assign opponent
        LC.Opponent = GetComponent<Player>();
        LC.Opponent.ID = int.Parse(LC.Opponent.netId.ToString());

        //disable non local components
        for (int i = 0; i < componentsToDisable.Length; i++)
        {
            componentsToDisable[i].enabled = false;
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        int i = NetworkServer.connections.Count;

        string _netID = GetComponent<NetworkIdentity>().netId.ToString();
        Player _player = GetComponent<Player>();

        GameController.RegisterPlayer(_netID, _player);
    }


    private void OnDisable()
    {
        if (sceneCamera != null)
        {
            sceneCamera.gameObject.SetActive(true);
        }

        GameController.UnRegisterPlayer(transform.name);
    }

}
