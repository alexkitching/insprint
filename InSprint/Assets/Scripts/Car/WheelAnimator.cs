﻿// Author: Alex Kitching
// Class Responsible for Animating InGame Wheels
using UnityEngine; 

public sealed class WheelAnimator : MonoBehaviour
{
    public float RotationSpeed;

    public Vector3 RotationDirection;

    private const float ROTATION_SPEED = 300f;

	private void Awake ()
	{
	    RotationSpeed = ROTATION_SPEED;
	}
	
	private void Update ()
	{
        transform.Rotate(RotationDirection, RotationSpeed * Time.deltaTime);
	}
}
