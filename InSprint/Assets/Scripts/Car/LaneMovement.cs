﻿//Author: Alec Brown

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class LaneMovement : NetworkBehaviour
{
    //scripts
    ServerMovement serverMovement;

    public static float laneChangeSpeed = 10f;

    //lane size
    public const int LaneSize = 2;
    public int CurrentLanexPos;
    public int CurrentLane;

    public int runCount = 0;

    [SyncVar]
    public bool isSwitching;

    private bool ServerChangeReady;
    public float ChangeLaneDelay = 0.3f;
    private float ChangeLaneTimer;

    private void Start()
    {
        if (!isServer) return;

        //get scripts
        serverMovement = GetComponent<ServerMovement>();
        //initialise the lane position
        CurrentLanexPos = (int)transform.position.x;

        //targetPos = transform.position;
        //set our server to ready
        ServerChangeReady = true;
        //initialise lane change speed
        ChangeLaneTimer = ChangeLaneDelay;

        //get starting lane pos
        switch (CurrentLanexPos)
        {
            case 0 - LaneSize:
                CurrentLane = 1;
                break;
            case 0:
                CurrentLane = 2;
                break;
            case 0 + LaneSize:
                CurrentLane = 3;
                break;
        }

        //set that on the server
        serverMovement.LanexPosition = CurrentLanexPos;

    }


    //update function to apply synced variable position locally
    private void LateUpdate()
    {
        //only update by local players
        if (isServer)
        {
            CountDownLaneChangeTimer();
        }
    }

    //stops instant lane changing
    void CountDownLaneChangeTimer()
    {
        if (!ServerChangeReady)
        {
            if (ChangeLaneTimer <= 0.0f)
            {
                ServerChangeReady = true;
                ChangeLaneTimer = ChangeLaneDelay;
            }
            else
            {
                ChangeLaneTimer -= Time.deltaTime;
            }
        }
    }


//SERVER SIDE FUNCTIONS

//update the target lane position synced variable on the server
    [Command]
    public void CmdSetLane(float InputVal)
    {
        if (!ServerChangeReady) return;

        ServerChangeReady = false;



        //check we can move
        if (!LocalLaneChangeRequest(InputVal)) return;
        //if (!serverMovement.LaneChangeRequest(player.gameObject, CurrentLanexPos + ((int)InputVal * LaneSize))) return;

        //if we have a positive input value go up a lane
        if (InputVal > 0)
        {
            if (CurrentLane <= 2)
            {
                    //set new lane pos (x val)
                    CurrentLanexPos += LaneSize;

                    serverMovement.LanexPosition = CurrentLanexPos;
                    //show position on server (debugging purposes TO REMOVE)
                    //transform.position = localMovement.targetPos;
                    //set lane pos
                    ++CurrentLane;
            }
        }
        //else go down a lane
        else if (InputVal < 0)
        {
            if (CurrentLane >= 2)
            {
                    //set new lane pos (x val)
                    CurrentLanexPos -= LaneSize;
                    //set target (synced)
                    //localMovement.targetPos = new Vector3(CurrentLanexPos, transform.position.y, transform.position.z);
                    serverMovement.LanexPosition = CurrentLanexPos;
                    //show position on server (debugging purposes TO REMOVE)
                    //transform.position = localMovement.targetPos;
                    //set lane pos
                    --CurrentLane;
            }
        }
    }



    private bool LocalLaneChangeRequest(float InputVal)
    {
        //cast a ray out to the side of the car - if it hits anything, we cant move there! (could also play an animation here)
        RaycastHit hit;

    //ray direction
    Vector3 rayDir = Vector3.zero;
        //Does the ray intersect any objects excluding the player layer
        if(InputVal > 0)
        {
            rayDir = transform.TransformDirection(Vector3.right);
        }
        else
        {
            rayDir = transform.TransformDirection(-Vector3.right);
        }

        if (Physics.Raycast(transform.position, rayDir, out hit, 3.4f))
        {
            Debug.DrawRay(transform.position, rayDir* hit.distance, Color.yellow);
Debug.Log("Did Hit");
            return false;
        }
        else
        {
            Debug.DrawRay(transform.position, rayDir* 5, Color.white);
            Debug.Log("Did not Hit");
            return true;
        }
    }

}
