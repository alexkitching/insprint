﻿// Author: Alex Kitching
// Class Responsible for Loading In Game Car Model
using Assets.Scripts.CarCustomisation;
using Assets.Scripts.Utility;
using UnityEngine;

public sealed class CarModelMaker
{
    private readonly GameObject _carBase;
    public CarLoadout Loadout;

    private readonly CarPartList _partList;

    private const string WHEEL_PIVOT_TAG = "WheelPivots";
    private const string DRIVER_PIVOT_TAG = "DriverPivot";

    public CarModelMaker(CarLoadout a_loadout, GameObject a_carBase)
    {
        _carBase = a_carBase;
        Loadout = a_loadout;

        _partList = Resources.Load<CarPartList>("PartList");

        if (_partList != null)
            return;

        Debug.Log("Unable to find part list!");
    }

    // Instantiates Model onto Carbase Parent
    public void BuildModel()
    {
        // Get Graphics Transform
        Transform graphics = _carBase.transform.GetChild(1); 
        // Instantiate Chassis
        GameObject chassis = Object.Instantiate(_partList.Chassis[Loadout.ChassisId].Prefab, graphics);

        foreach (Transform child in chassis.transform)
        {
            switch (child.gameObject.tag)
            {
                case WHEEL_PIVOT_TAG: // Instantiate Wheels
                    foreach (Transform wheelPivot in child) 
                    {
                        GameObject wheelObject = Object.Instantiate(_partList.Wheels[Loadout.WheelId].Prefab, wheelPivot);

                        WheelAnimator animator = wheelObject.AddComponent<WheelAnimator>();

                        if (wheelPivot.name.Contains("Left"))
                        {
                            wheelObject.transform.Rotate(0f, 180f, 0f);
                            animator.RotationDirection = Vector3.right;
                        }
                        else
                        {
                            animator.RotationDirection = -Vector3.right;
                        }
                    }

                    break;
                case DRIVER_PIVOT_TAG: // Instantiate Driver
                    Object.Instantiate(_partList.Drivers[Loadout.DriverId].Prefab, child);
                    break;
            }
        }

        // Rotate and Scale Model
        graphics.Rotate(0f, 180f, 0f);
        graphics.localScale = new Vector3(0.85f, 0.85f, 0.85f);

        CarTransUtil.DeleteMeshesOnTransformandChild(graphics);
    }
}
