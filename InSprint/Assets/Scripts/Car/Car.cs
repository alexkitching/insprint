﻿// Author: Alex Kitching (Initial), Alec Brown
using Assets.Scripts.SprintInput;
using UnityEngine;
using UnityEngine.Networking;


[RequireComponent(typeof(InputHandler))]
public class Car : NetworkBehaviour
{
    public float CurrentVelocity;
    public float DefaultVelocity = 20.0f;
    public bool Speedboost;
    public bool Slowdown; //Added speed debuff variable
    public float BoostAmt;
    public float BoostTime;

    public KeyCode BoostKey = KeyCode.Space;
    public KeyCode SlowKey = KeyCode.LeftControl;

    ServerMovement serverMovement;
    LaneMovement laneMovement;

    private InputHandler input;

    private void Awake()
    {
        input = GetComponent<InputHandler>();
    }

    // Use this for initialization
    void Start()
    {
        //get scripts
        serverMovement = GetComponent<ServerMovement>();
        laneMovement = GetComponent<LaneMovement>();
        //set initial velocity
        CurrentVelocity = DefaultVelocity;
    }

    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer)
        {
            RacePosition();
            MoveLane();
        }
    }

    //set lane position on local games
    private void MoveLane()
    {
        if (input.Horizontal != 0)
        {
            laneMovement.CmdSetLane(input.Horizontal);
        }
    }


    public void RacePosition()
    {
        //direction multiplier //fwd = +1, backwards = -1
        int dir = 0;

        // DEBUGGING KEYS /////////////////////////////////
        //wont work on mobile so left in for pc testing
        if (Input.GetKeyDown(BoostKey))
        {
            Speedboost = true;
        }
        if (Input.GetKeyDown(SlowKey))
        {
            Slowdown = true;
        }

        //////////////////////////////////////////////////

        //function to boost car
        if (Speedboost)
        {
            Speedboost = false;
            dir = 1;
        }

        //function to slow down car
        if(Slowdown)
        {
            Slowdown = false;
            dir = -1;
        }

        //update position on the server
        if (dir != 0)
        {
            serverMovement.CmdUpdatePosition(dir);
            dir = 0;
        }

    }
}
