﻿//Author: Alec Brown

using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Assets.Scripts.Managers;

public class Player : NetworkBehaviour
{
    public int ID;

    //variables to show game win/loss
    [SyncVar]
    private bool _hasWon = false;
    public bool hasWon
    {
        get { return _hasWon; }
        set { _hasWon = value; }
    }

    [SyncVar]
    private bool _hasLost = false;
    public bool hasLost
    {
        get { return _hasLost; }
        set { _hasLost = value; }
    }

    //behaviour
    [SerializeField]
    private Behaviour[] disableOnWin;
    private bool[] WasEnabled;

    public int Coins { get; set; }

    public void SetUp()
    {
        //enable all behaviours (player controls mainly) on setup
        WasEnabled = new bool[disableOnWin.Length];
        for (int i = 0; i < WasEnabled.Length; i++)
        {
            WasEnabled[i] = disableOnWin[i].enabled;
        }
        SetDefaults();
    }

    public void Win()
    {
        if (!hasWon) //only run once
        {
            hasWon = true;

            //Disable components
            for (int i = 0; i < disableOnWin.Length; i++)
            {
                disableOnWin[i].enabled = false;
            }

            Collider _col = GetComponent<Collider>();
            if (_col != null)
            {
                _col.enabled = false;
            }

            //log win
            Debug.Log("You Won!");

            GameController.Instance.endRacePanel.SetActive(true);

            GameController.Instance.endRaceText.text = "You Won!";

            StartCoroutine(LoadMenu());
        }
    }

    public void Lose()
    {
        if (!hasWon) //only run once
        {
            hasLost = true;

            //Disable components
            for (int i = 0; i < disableOnWin.Length; i++)
            {
                disableOnWin[i].enabled = false;
            }

            Collider _col = GetComponent<Collider>();
            if (_col != null)
            {
                _col.enabled = false;
            }

            //log win
            Debug.Log("You Lost!");


            GameController.Instance.endRacePanel.SetActive(true);

            GameController.Instance.endRaceText.text = "You Lost!";

            StartCoroutine(LoadMenu());
        }
    }

    public void SetDefaults()
    {
        hasWon = false;

        for (int i = 0; i < disableOnWin.Length; i++)
        {
            disableOnWin[i].enabled = WasEnabled[i];
        }

        Collider _col = GetComponent<Collider>();
        if (_col != null)
        {
            _col.enabled = true;
        }
    }

    IEnumerator LoadMenu()
    {
        //hang in the game screen for x seconds
        yield return new WaitForSeconds(5.0f);

        //delete the game (road segments etc)
        //Destroy(GameObject.Find("Game"));// this didnt work..
        // GameOverPanel.SetActive(false);

        //disconnect from the network
        NetworkManager.singleton.StopClient();
        NetworkManager.singleton.StopHost();

        NetworkLobbyManager.singleton.StopClient();
        NetworkLobbyManager.singleton.StopServer();

        NetworkServer.DisconnectAll();
        Destroy(NetworkLobbyManager.singleton.gameObject);

        //load the main menu back up
        SceneManager.LoadScene(1);
    }
}
