﻿//Author: Alec Brown

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Assets.Scripts.Managers;

public class LocalMovement : NetworkBehaviour
{
    public float OpponentsPosition;
    public float OpponentDistance;

    public float boostVelocity;
    public float dragVelocity;
    public float targetVelocity;

    bool bJustHit = false;


    ServerMovement serverMovement;
    Car car;

    // Use this for initialization
    void Start () {
        //targetPos = transform.position;
        serverMovement = GetComponent<ServerMovement>();
        car = GetComponent<Car>();
        serverMovement.playerDistance = 0.0f;

        //set velocity values
        targetVelocity = car.DefaultVelocity;
        boostVelocity = car.DefaultVelocity + car.BoostAmt / car.BoostTime;
        dragVelocity = car.DefaultVelocity - car.BoostAmt / car.BoostTime;

    }
	
	// Updates our cars to their respective target positions
	void Update ()
    {
        if (GameController.Instance.LevelController.Opponent == null ||
            GameController.Instance.LevelController.LocalPlayer == null)
            return;
        UpdatePosition();
        UpdateVelocity();
    }


    //local player function to update lane position
    void UpdatePosition()
    {
        //get my opponent
        Player opponent = GameController.Instance.LevelController.Opponent;
        //get a reference to me
        Player localPlayer = GameController.Instance.LevelController.LocalPlayer;


        //work out which player I am to determine wether my player distance should be reversed
        if (opponent.ID > localPlayer.ID)
        {
            OpponentDistance = -serverMovement.playerDistance;
        }
        else
        {
            OpponentDistance = serverMovement.playerDistance;
        }

        Vector3 pos = transform.position;
        float step;

        step = LaneMovement.laneChangeSpeed * Time.deltaTime;

        //update lane change by target pos
        transform.position = Vector3.MoveTowards(pos, new Vector3(serverMovement.LanexPosition, pos.y, pos.z), step);
        //update opponents lane changes too
        pos = opponent.transform.position;
        opponent.transform.position = Vector3.MoveTowards(pos, new Vector3(opponent.GetComponent<ServerMovement>().LanexPosition, pos.y, pos.z), step);

        //calc boost duration
        step = (car.BoostAmt / car.BoostTime) * Time.deltaTime;

        pos = opponent.transform.position;
        //update my opponents position based on the distance between us on the server
        opponent.transform.position = Vector3.MoveTowards(pos, new Vector3(pos.x, pos.y, OpponentDistance), step);
        Debug.Log("Player distance is: " + -OpponentDistance);


        //cast a ray out infront of the car - if it hits anything, sent us backwards again! (could also play an animation here)
        RaycastHit hit;
        //Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 2.5f))
        {
            if (hit.transform.tag == "Player")
            {
                if (!bJustHit)
                {
                    bJustHit = true;
                    Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                    Debug.Log("Did Hit");
                    car.Slowdown = true;
                }
            }
        }
        else
        {
            bJustHit = false;
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 100, Color.white);
            Debug.Log("Did not Hit");
        }
    }


    [ClientRpc]
    public void RpcUpdateVelocity(int Dir)
    {
        if (car == null)
            return;
        //stop any currently running Coroutines
        StopCoroutine("resetVelocity");

        //apply relative velocity
        if (Dir > 0)
        {
            targetVelocity = boostVelocity;
        }
        else
        {
            targetVelocity = dragVelocity;
        }
        //start new Coroutine to reset velocity
        StartCoroutine(resetVelocity());
    }


    IEnumerator resetVelocity()
    {
        yield return new WaitForSeconds(car.BoostTime);
        //reset veolicty to default vel
        targetVelocity = car.DefaultVelocity;
    }


    void UpdateVelocity()
    {
        //smoth step lerps the velocity in and out to stop sudden change 
        // if there's any difference in syncing power ups this will be why!
        float SmoothMultiplier = 20.0f;

        //if there is a differece in sync, comment out the IF statement below and just change the current vel to the target vel immediately.

        //car.CurrentVelocity = targetVelocity;

        if (car.CurrentVelocity != targetVelocity)
        {
            float step = SmoothMultiplier * Time.deltaTime;
            car.CurrentVelocity = Mathf.SmoothStep(car.CurrentVelocity, targetVelocity, step);
        }
    }
}
