﻿﻿//Author: Alec Brown
using UnityEngine;
using UnityEngine.Networking;
using Assets.Scripts.Managers;

//taken from Pav's power up movement - ammended to take into consideration player movement post-instantiation of finish line
public class FinishLineMovement : NetworkBehaviour
{
    private LevelController levelController;
    private Car localPlayerCar;



    void Start()
    {
        levelController = GameController.Instance.LevelController;
        localPlayerCar = levelController.LocalPlayer.GetComponent<Car>();
    }

    void Update()
    {
        UpdateMovement();
    }

    //update our finish lines movement base on local players velocity
    private void UpdateMovement()
    {
        float zAxisVelocity = -localPlayerCar.CurrentVelocity * Time.deltaTime;
        transform.position += new Vector3(0f, 0f, zAxisVelocity);

        if (transform.position.z < -10f)
        {
            //get my opponent
            Player opponent = GameController.Instance.LevelController.Opponent;

            if (opponent.transform.position.z < 0.0f)
            {
                GameController.Instance.LevelController.LocalPlayer.Win();
            }
            else
            {
                GameController.Instance.LevelController.LocalPlayer.Lose();
            }
            Destroy(gameObject);
        }
    }
}
