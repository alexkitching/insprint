﻿//Author: Alec Brown

using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Managers;
using UnityEngine.Networking;
using UnityEngine;

public class FinishLine : NetworkBehaviour
{
    public GameObject _FinishLinePrefab;
    public Transform FinishLineLocation;
    public bool GameEnd = false;

    private LevelController levelController;

    private void Start()
    {
        levelController = GameController.Instance.LevelController;
    }

    private void Update()
    {
        //server side
        if (!isServer || levelController.Opponent == null)
            return;

        CheckFinishLine();
    }

    //check to see if we're at the end of the racelength and we need a finish line spawning
    private void CheckFinishLine()
    {
        float OpponentDistance = levelController.LocalPlayer.GetComponent<LocalMovement>().OpponentDistance;
        if ((OpponentDistance <= -GameController.Instance.matchSettings.RaceLength ||
            OpponentDistance >= GameController.Instance.matchSettings.RaceLength) && !GameEnd)
        {
            GameEnd = true;
            StartCoroutine(CORLoadFinishLine());
        }
    }

    IEnumerator CORLoadFinishLine()
    {
        //show finish line after the boost
        yield return new WaitForSeconds(GameController.Instance.LevelController.LocalPlayer.GetComponent<Car>().BoostTime);

        //load the finish line on the server
        LoadFinishLine();
    }


    void LoadFinishLine()
    {
        //spawn it on all clients
        RpcSpawnFinishLine(FinishLineLocation.position);
    }

    [ClientRpc]
    void RpcSpawnFinishLine(Vector3 SpawnPoint)
    {
        //calc final spawnpoint position
        if (levelController != null && levelController.Opponent != null)
        {
            float distanceBetweenPlayers = levelController.LocalPlayer.GetComponent<LocalMovement>().OpponentDistance;
            if (distanceBetweenPlayers > 0)
            {
                SpawnPoint += new Vector3(0, 0, distanceBetweenPlayers);
            }
        }

        //create a finish line on all clients
        Instantiate(_FinishLinePrefab, SpawnPoint, Quaternion.identity);

    }
}