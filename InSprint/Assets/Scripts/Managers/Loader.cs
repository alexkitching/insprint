﻿// Author: Alex Kitching
// Class Responsible for Loading Level Scenery
using Assets.Scripts.ProceduralScenery;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public sealed class Loader : MonoBehaviour
    {
        [SerializeField]
        private GameObject _segment = null;

        [SerializeField]
        private ServerBiomeHandler _biomeHandler;

        public float SegmentLength = 0f;

        public float RoadLength;

        public int NumSegments = -1;

        private void Awake()
        {
            _biomeHandler.SetupStartBiome();
        }

        // Loads Level Scenery
        public void LoadLevel(LevelController a_levelController)
        {
            // Load Moving Scene Assets
            Vector3 roadPos = Vector3.zero;

            for (int i = 0; i < NumSegments; i++)
            {
                a_levelController.MovingSegments[i] = Instantiate(_segment, roadPos, _segment.transform.rotation, a_levelController.transform);
                roadPos.z += SegmentLength;
                GenerateSegmentScenery(a_levelController.MovingSegments[i]);
            }
        }

        // Called via reset scenery of Level Controller, causes new Scenery Assets to be generated
        public void GenerateSegmentScenery(GameObject a_segment)
        {
            foreach (Transform child in a_segment.GetComponent<Section>().leftSceneryHolder)
            {
                Destroy(child.gameObject);
            }
            foreach (Transform child in a_segment.GetComponent<Section>().rightSceneryHolder)
            {
                Destroy(child.gameObject);
            }

            // Generate Left Scenery
            Instantiate(_biomeHandler.CurrentBiome.GetSceneryObject(), a_segment.GetComponent<Section>().leftSceneryHolder.position, Quaternion.identity, a_segment.GetComponent<Section>().leftSceneryHolder);
            // Generate Right Scenery
            Instantiate(_biomeHandler.CurrentBiome.GetSceneryObject(), a_segment.GetComponent<Section>().rightSceneryHolder.position, Quaternion.Euler(0, 180, 0), a_segment.GetComponent<Section>().rightSceneryHolder);
        }
    }
}
