﻿// Author: Alex Kitching (Initial and Level Controller), Alec Brown (Player Registration), Pawel Granda (EndRace, Coins)
// Singleton Class Responsible for Creating the LevelController on scene load which initialises the level
using System.Collections.Generic;
using Assets.Scripts.SprintDebug;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Managers
{
    [RequireComponent(typeof(Loader))]
    public sealed class GameController : MonoBehaviour
    {
        public static GameController Instance;

        private Loader _loader = null;

        private LevelController _levelController;
        public LevelController LevelController { get { return _levelController; } }

        public MatchSettings matchSettings;

        public InputDebugWindow debugWindow;

        public Text CoinsText;

        public GameObject endRacePanel;

        public Text endRaceText;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(this);
            }

            _loader = GetComponent<Loader>();
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        // Called when Scene is finished loading
        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            // Create Level Controller
            GameObject levelControllerObj = new GameObject("Game", typeof(LevelController));
            // LevelControllerObj.AddComponent<FinishLine>();
            _levelController = levelControllerObj.GetComponent<LevelController>();
            // Initialise Level
            _levelController.Initialise(_loader);
            Debug.Log(scene.name + " loaded.");
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        
        #region Player Registration

        private const string PLAYER_ID_PREFIX = "Player ";

        private static Dictionary<string, Player> players = new Dictionary<string, Player>();

        public static void RegisterPlayer( string _netID, Player _player)
        {
            string _playerID = PLAYER_ID_PREFIX + _netID;
            players.Add(_playerID, _player);
            _player.transform.name = _playerID;
        }

        public static void UnRegisterPlayer(string _playerID)
        {
            players.Remove(_playerID);
        }

        public static Player GetPlayer(string _playerID)
        {
            if (!players.ContainsKey(_playerID))
            {
                return null;
            }

            return players[_playerID];
        }


        //GUI to show players and player names in the dictionary
        //private void OnGUI()
        //{
        //    GUILayout.BeginArea(new Rect(200,200,200,500));
        //    GUILayout.BeginVertical();

        //    foreach (string _playerID in players.Keys)
        //    {
        //        GUILayout.Label(_playerID + "  -  " + players[_playerID].transform.name);
        //    }

        //    GUILayout.EndVertical();
        //    GUILayout.EndArea();
        //}

        #endregion
    }
}
