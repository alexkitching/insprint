﻿// Author: Alex Kitching (Initial, Moving Scenery), Pawel Granda (Spawned Missiles and Oil)
// Class responsible for loading Level and Moving Scenery
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public sealed class LevelController : MonoBehaviour
    {
        private Loader _loader = null;

        [SerializeField]
        public Player LocalPlayer;

        [SerializeField]
        public Player Opponent;

        public Dictionary<int, string> spawnedMissiles { get; set; }
        public Dictionary<int, string> spawnedOils { get; set; }
    
        //public variables
        public Vector3 VelocityVec;

        public GameObject[] MovingSegments;

        public float DistanceBetween;

        // Use this for initialization
        void Awake ()
        {
            spawnedMissiles = new Dictionary<int, string>();
            spawnedOils = new Dictionary<int, string>();
            MovingSegments = new GameObject[4];
        }

        // Initialises Level
        public void Initialise(Loader a_loader)
        {
            _loader = a_loader;
            MovingSegments = new GameObject[_loader.NumSegments];

            _loader.LoadLevel(this);
        }

        // Update is called once per frame
        void Update ()
        {
            // Moves Scenery
            MoveScenery();
        }

        private void MoveScenery()
        {
            if (!LocalPlayer) return; //stop warning errors before LocalPlayer is initialised

            // Move Scenery based on Local Players Cars Velocity
            float zVelocity = -LocalPlayer.GetComponent<Car>().CurrentVelocity * Time.deltaTime;
            VelocityVec = new Vector3(0f, 0f, zVelocity);

            foreach (GameObject segment in MovingSegments)
            {
                segment.transform.position += VelocityVec;

                if (segment.transform.position.z < -_loader.SegmentLength)
                {
                    ResetSegment(segment);
                }
            }
        }

        // Resets Segment when offscreen
        private void ResetSegment(GameObject a_segment)
        {
            float remainder = a_segment.transform.position.z + _loader.SegmentLength;
            Vector3 newPos = a_segment.transform.position;
            newPos.z = _loader.RoadLength + remainder;
            a_segment.transform.position = newPos;

            _loader.GenerateSegmentScenery(a_segment);
        }
    }
}
