﻿//Author: Alec Brown

[System.Serializable]

public class MatchSettings
{
    //default speed of the road movement
    public float DefaultVelocity = 20f;
    public float MaxVelocity = 50f;
    public float MinVelocity = 5f;
    //velocity multiplier get multipled by the boost amount to make the road look like its moving faster than it is during boosts
    public float BoostVelocityMultiplier = 5f;
    //velocity multiplier get multipled by the boost amount to slow down the scene when experiencing drag
    public float DragVelocityMultiplier = 2.5f;
    //player start position
    public const int startPosition = 0;
    //race length
    public float RaceLength = 75;
    //total road length
    public float RoadLength = 120;
}
