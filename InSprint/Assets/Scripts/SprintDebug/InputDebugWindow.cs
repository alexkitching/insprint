﻿// Author: Alex Kitching
// Class Responsible for Updating Input Debug Window for use with Testing Mobile Input

using System.Globalization;
using Assets.Scripts.SprintInput;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.SprintDebug
{
    public sealed class InputDebugWindow : MonoBehaviour
    {
        private InputHandler _input;

        [SerializeField]
        private Text _horizontalText;
        [SerializeField]
        private Text _rotationRateText;
        [SerializeField]
        private Text _gravityText;
        [SerializeField]
        private Text _attitudeText;
        [SerializeField]
        private Text _accelerationText;

        public void Setup(InputHandler a_input)
        {
            _input = a_input;

            if (Input.gyro.enabled) return;
            _rotationRateText.text = "UNAVAILABLE";
            _gravityText.text = "UNAVAILABLE";
            _attitudeText.text = "UNAVAILABLE";
            _accelerationText.text = "UNAVAILABLE";
        }
	
        private void Update ()
        {
            if (_input == null) return;

            if(Input.gyro.enabled) // Gyro is Enabled, update Inputs on window
            {
                _rotationRateText.text = _input.GyroRotationRate.ToString();
                _gravityText.text = _input.GyroGravity.ToString();
                _attitudeText.text = _input.GyroAttitude.ToString();
                _accelerationText.text = _input.Acceleration.ToString();
            }
            _horizontalText.text = _input.Horizontal.ToString(CultureInfo.InvariantCulture);
        }
    }
}
