﻿// Author: Alex Kitching
// Scriptable Object Class for creating lists of available car parts, used for easy interface for adding/changing parts in part database.
using UnityEngine;

namespace Assets.Scripts.CarCustomisation
{
    [CreateAssetMenu]
    public sealed class CarPartList : ScriptableObject
    {
        public Chassis[] Chassis;
        public Driver[] Drivers;
        public Wheel[] Wheels;
    }
}
