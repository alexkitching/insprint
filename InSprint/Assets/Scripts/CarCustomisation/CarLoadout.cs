﻿// Author: Alex Kitching
// Struct which holds relevant loadout data for use with saving and loading loadouts
namespace Assets.Scripts.CarCustomisation
{
    public struct CarLoadout
    {
        public int ChassisId;
        public int DriverId;
        public int WheelId;
        public string Name;

        public CarLoadout(int a_chassisId, int a_driverId, int a_wheelId, string a_name)
        {
            ChassisId = a_chassisId;
            DriverId = a_driverId;
            WheelId = a_wheelId;
            Name = a_name;
        }
    }
}
