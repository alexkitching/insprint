﻿// Author: Alex Kitching
// Inherited Scriptable Object Class for Chassis Parts
using UnityEngine;

namespace Assets.Scripts.CarCustomisation
{
    [CreateAssetMenu(fileName = "Chassis", menuName = "CarParts/Chassis")]
    public sealed class Chassis : CarPart
    {
        public int WheelCount;
    }
}
