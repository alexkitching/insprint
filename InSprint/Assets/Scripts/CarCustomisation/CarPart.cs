﻿// Author: Alex Kitching
// Scriptable Object Class base for various Car Parts
using UnityEngine;

namespace Assets.Scripts.CarCustomisation
{
    public abstract class CarPart : ScriptableObject
    {
        public string Name;
        public GameObject Prefab;
        public int Cost;
    }
}
