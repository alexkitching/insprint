﻿// Author: Alex Kitching
// Inherited Scriptable Object Class for Wheel Parts
using UnityEngine;

namespace Assets.Scripts.CarCustomisation
{
    [CreateAssetMenu(fileName = "Wheel", menuName = "CarParts/Wheels")]
    public sealed class Wheel : CarPart
    {
    }
}
