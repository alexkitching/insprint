﻿// Author: Alex Kitching
// Inherited Scriptable Object Class for Driver Parts
using UnityEngine;

namespace Assets.Scripts.CarCustomisation
{
    [CreateAssetMenu(fileName = "Driver", menuName = "CarParts/Drivers")]
    public sealed class Driver : CarPart
    {
    }
}
