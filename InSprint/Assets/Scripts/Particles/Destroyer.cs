﻿// Author: Pawel Granda
// Script that destroys gameobject after 6 seconds (used to destroy particles)
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Particles
{
    public class Destroyer : NetworkBehaviour
    {
        void Start()
        {
            Destroy(gameObject, 6f);
        }

    }
}
