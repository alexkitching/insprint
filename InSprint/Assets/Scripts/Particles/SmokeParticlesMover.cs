﻿// Author: Pawel Granda
// Script that is moving smoke particles according to the local player car velocity

using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Particles
{
    public class SmokeParticlesMover : NetworkBehaviour
    {
        private LevelController levelController;

        void Start ()
        {
            levelController = GameController.Instance.LevelController;
        }

        void Update()
        {
            UpdateMovement();
        }

        private void UpdateMovement()
        {
            float zAxisVelocity = -levelController.LocalPlayer.gameObject.GetComponent<Car>().CurrentVelocity * Time.deltaTime;
            transform.position += new Vector3(0f, 0f, zAxisVelocity);


            Destroy(gameObject, 6f);
        }
    }
}
