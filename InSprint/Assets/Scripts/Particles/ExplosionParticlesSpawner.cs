﻿// Author: Pawel Granda
// Script taht is spawning explosion particles for both players

using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Particles
{
    public class ExplosionParticlesSpawner : NetworkBehaviour
    {
        private LevelController levelController;

        public GameObject ExplosionGameObject;

        void Start()
        {
            levelController = GameController.Instance.LevelController;
        }

        [Command]
        public void CmdSpawnExplosion(int spawningPlayerID)
        {
            RpcSpawnExplosion(spawningPlayerID);
        }

        [ClientRpc]
        public void RpcSpawnExplosion(int spawningPlayerID)
        {
            Vector3 spawnPointPosition = new Vector3();

            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                // Get the position and name of the player in the local game
                if (spawningPlayerID == player.GetComponent<Player>().ID)
                {
                    spawnPointPosition = player.transform.position;
                }
            }

            var explosion = Instantiate(ExplosionGameObject, spawnPointPosition, Quaternion.identity);
        }
    }
}
