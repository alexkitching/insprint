﻿// Author: Pawel Granda
// Script that is spawning smoke particles for both players

using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Particles
{
    public class SmokeParticlesSpawner : NetworkBehaviour
    {
        private LevelController levelController;

        public GameObject SpeedSmokeGameObject;

        void Start()
        {
            levelController = GameController.Instance.LevelController;
        }

        [Command]
        public void CmdSpawnSpeedSmoke(int spawningPlayerID)
        {
            RpcSpawnSpeedSmoke(spawningPlayerID);
        }

        [ClientRpc]
        public void RpcSpawnSpeedSmoke(int spawningPlayerID)
        {
            Vector3 spawnPointPosition = new Vector3();

            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                // Get the position and name of the player in the local game
                if (spawningPlayerID == player.GetComponent<Player>().ID)
                {
                    spawnPointPosition = player.transform.position;
                }
            }

            var smoke = Instantiate(SpeedSmokeGameObject, spawnPointPosition, Quaternion.identity);
        }
    }
}
