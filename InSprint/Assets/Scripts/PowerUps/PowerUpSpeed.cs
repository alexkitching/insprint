﻿// Author: Pawel Granda
// Class that represents speed buff power up, it contains specific implementation for activating this type of power up

using Assets.Scripts.Managers;
using Assets.Scripts.Particles;

namespace Assets.Scripts.PowerUps
{
    public class PowerUpSpeed : PowerUp
    {
        private LevelController levelController;

        private SmokeParticlesSpawner speedSmokeSpawner;

        public override void ActivatePowerUp()
        {
            levelController = GameController.Instance.LevelController;
            Player.gameObject.GetComponent<Car>().Speedboost = true;
            speedSmokeSpawner = levelController.LocalPlayer.GetComponent<Car>().GetComponent<SmokeParticlesSpawner>();
            speedSmokeSpawner.CmdSpawnSpeedSmoke(levelController.LocalPlayer.ID);
        }

        public PowerUpSpeed(Player player) : base("Speedboost", player)
        {

        }
    }
}
