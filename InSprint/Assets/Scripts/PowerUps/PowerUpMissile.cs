﻿// Author: Pawel Granda
// Class that represents missile power up, it contains specific implementation for activating this type of power up

using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.PowerUps
{
    public class PowerUpMissile : PowerUp
    {
        private LevelController levelController;

        private PowerUpMissileSpawner missileSpawner;

        public override void ActivatePowerUp()
        {
            levelController = GameController.Instance.LevelController;
            missileSpawner = levelController.LocalPlayer.GetComponent<Car>().GetComponent<PowerUpMissileSpawner>();

            missileSpawner.CmdSpawnMissile(levelController.LocalPlayer.ID);
        }

        public PowerUpMissile(Player player) : base("Missile", player)
        {

        }
    }
}
