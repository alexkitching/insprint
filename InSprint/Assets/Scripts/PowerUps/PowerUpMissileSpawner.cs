﻿// Author: Pawel Granda
// Script that is spawning missile

using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.PowerUps
{
    public class PowerUpMissileSpawner : NetworkBehaviour
    {
        [SerializeField]
        private GameObject missilePrefab;

        private LevelController levelController;

        void Start()
        {
            levelController = GameController.Instance.LevelController;
        }

        [Command]
        public void CmdSpawnMissile(int spawningPlayerID)
        {

            RpcSpawnMissile(spawningPlayerID);
        }

        [ClientRpc]
        private void RpcSpawnMissile(int spawningPlayerID)
        {
            Vector3 spawnPointPosition = new Vector3();
            string spawningPlayerName = null;

            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                // Get the position and name of the player in the local game
                if(spawningPlayerID == player.GetComponent<Player>().ID)
                {
                    spawnPointPosition = player.transform.position;
                    spawningPlayerName = player.name;
                }
            }

            var missile = Instantiate(missilePrefab, spawnPointPosition, Quaternion.identity);
            if (!levelController.spawnedMissiles.ContainsKey(missile.GetInstanceID()))
            {
                Debug.Log(string.Format("Name: {0}, tag: {1}, ID: {2}", missile.name, missile.tag, missile.GetInstanceID()));
                levelController.spawnedMissiles.Add(missile.GetInstanceID(), spawningPlayerName);
            }
        }
    }
}
