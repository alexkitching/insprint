﻿// Author: Pawel Granda
// Script that is moving and handling collision on missile
using System.Linq;
using Assets.Scripts.Managers;
using Assets.Scripts.Particles;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.PowerUps
{
    public class PowerUpMissileMover : NetworkBehaviour
    {
        private LevelController levelController;

        private ExplosionParticlesSpawner explosionParticles;

        void Start()
        {
            levelController = GameController.Instance.LevelController;
            explosionParticles = levelController.LocalPlayer.GetComponent<Car>().GetComponent<ExplosionParticlesSpawner>();
        }

        void Update()
        {
            UpdateMovement();
        }

        void OnTriggerEnter(Collider other)
        {

            var element = levelController.spawnedMissiles.FirstOrDefault(el => el.Key.Equals(gameObject.GetInstanceID()));
            if (element.Value.Equals(other.gameObject.GetComponent<Player>().name))
            {
                return;
            }

            if (!element.Value.Equals(levelController.LocalPlayer.name))
            {
                levelController.LocalPlayer.gameObject.GetComponent<Car>().Slowdown = true;
                explosionParticles.CmdSpawnExplosion(levelController.LocalPlayer.ID);
            }

            Destroy(gameObject);
            levelController.spawnedMissiles.Remove(gameObject.GetInstanceID());
        }

        private void UpdateMovement()
        {
            float zAxisVelocity = levelController.LocalPlayer.gameObject.GetComponent<Car>().CurrentVelocity * Time.deltaTime;
            transform.position += new Vector3(0f, 0f, zAxisVelocity);

            if (transform.position.z > 140f)
            {
                levelController.spawnedMissiles.Remove(gameObject.GetInstanceID());
                Destroy(gameObject);
            }
        }
    }
}
