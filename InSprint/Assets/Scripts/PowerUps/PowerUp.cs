﻿// Author: Pawel Granda
// Base class for all kinds of power ups
using UnityEngine;

namespace Assets.Scripts.PowerUps
{
    public abstract class PowerUp
    {
        public abstract void ActivatePowerUp();

        public string Name { get; private set; }

        protected Player Player;

        protected PowerUp(string name, Player player)
        {
            this.Name = name;
            this.Player = player;
        }
    }
}
