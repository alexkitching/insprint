﻿// Author: Pawel Granda
// Class that represents oil spill power up, it contains specific implementation for activating this type of power up

using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.PowerUps
{
    public class PowerUpOil : PowerUp
    {
        private LevelController levelController;

        private PowerUpOilSpawner oilSpawner;

        public override void ActivatePowerUp()
        {
            levelController = GameController.Instance.LevelController;
            oilSpawner = levelController.LocalPlayer.GetComponent<Car>().GetComponent<PowerUpOilSpawner>();

            oilSpawner.CmdSpawnOil(levelController.LocalPlayer.ID);
        }

        public PowerUpOil(Player player) : base ("Oil spill", player)
        {

        }
    }
}
