﻿// Author: Pawel Granda
// Class that represents slow down power up (power down ;D), it contains specific implementation for activating this type of power up
namespace Assets.Scripts.PowerUps
{
    public class PowerUpSlow : PowerUp
    {
        public override void ActivatePowerUp()
        {
            Player.gameObject.GetComponent<Car>().Slowdown = true;
        }

        public PowerUpSlow(Player player) : base("Slowdown", player)
        {

        }
    }
}
