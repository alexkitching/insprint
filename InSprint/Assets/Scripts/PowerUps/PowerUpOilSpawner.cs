﻿// Author: Pawel Granda
// Script that is spawning oil spill

using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.PowerUps
{
    public class PowerUpOilSpawner : NetworkBehaviour
    {
        [SerializeField]
        private GameObject oilPrefab;

        private LevelController levelController;

        void Start()
        {
            levelController = GameController.Instance.LevelController;
        }

        [Command]
        public void CmdSpawnOil(int spawningPlayerID)
        {
            RpcSpawnOil(spawningPlayerID);
        }

        [ClientRpc]
        private void RpcSpawnOil(int spawningPlayerID)
        {

            Vector3 spawnPointPosition = new Vector3();
            string spawningPlayerName = null;

            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                // Get the position and name of the player in the local game
                if (spawningPlayerID == player.GetComponent<Player>().ID)
                {
                    spawnPointPosition = player.transform.position;
                    spawningPlayerName = player.name;
                }
            }

            var oil = Instantiate(oilPrefab, spawnPointPosition, Quaternion.identity);
            if (!levelController.spawnedOils.ContainsKey(oil.GetInstanceID()))
            {
                levelController.spawnedOils.Add(oil.GetInstanceID(), spawningPlayerName);
            }
        }
    }
}
