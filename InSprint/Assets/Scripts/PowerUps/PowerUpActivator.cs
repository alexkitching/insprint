﻿// Author: Pawel Granda
// Class that is handling power up activation process (displaying UI information to Player after collecting Power Up, and passing activation information to the power up)
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Assets.Scripts.PowerUps
{
    public sealed class PowerUpActivator : MonoBehaviour
    {
        public static PowerUpActivator Instance;

        private PowerUp powerUp;

        [SerializeField]
        private Button powerUpButton;

        [SerializeField]
        private Text powerUpButtonText;

        [SerializeField]
        private GameObject powerUpPickUpPanel;

        [SerializeField]
        private Text powerUpPickUpText;

        void Awake()
        {
            if (Instance != null)
            {
                Debug.LogError("PowerUpActivator Instance Problem!");
            }
            else
            {
                Instance = this;
            }

            PowerUp = null;
        }

        public PowerUp PowerUp
        {
            get { return powerUp; }
            set
            {
                powerUp = value;
                if (powerUp != null)
                {
                    Instance.powerUpButton.enabled = true;
                    Instance.powerUpButtonText.text = PowerUp.Name;
                    Instance.powerUpPickUpPanel.SetActive(true);
                    Instance.powerUpPickUpText.text = "You picked up: " + PowerUp.Name;
                    StartCoroutine(WaitForFewSeconds());
                }
                else
                {
                    Instance.powerUpButton.enabled = false;
                    Instance.powerUpButtonText.text = string.Format("No PowerUp!");
                }
            }
        }

        public void ActivatePowerUp()
        {
            if (PowerUp != null)
            {
                PowerUp.ActivatePowerUp();
                PowerUp = null;
            }
        }

        IEnumerator WaitForFewSeconds()
        {
            yield return new WaitForSeconds(3);
            Instance.powerUpPickUpPanel.SetActive(false);
        }
    }

}
