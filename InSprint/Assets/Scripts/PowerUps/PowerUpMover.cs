﻿// Author: Pawel Granda
// Script that is moving the power up boxes (uncollected power ups) according to the local player velocity

using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.PowerUps
{
    public class PowerUpMover : NetworkBehaviour
    {
        private LevelController levelController;
        private Car localPlayerCar;

        void Start()
        {
            levelController = GameController.Instance.LevelController;
            localPlayerCar = levelController.LocalPlayer.GetComponent<Car>();
        }

        void Update()
        {
            UpdateMovement();
        }

        private void UpdateMovement()
        {
            float zAxisVelocity = -localPlayerCar.CurrentVelocity * Time.deltaTime;
            transform.position += new Vector3(0f, 0f, zAxisVelocity);

            if (transform.position.z < -10f)
            {
                Destroy(gameObject);
            }
        }
    }
}
