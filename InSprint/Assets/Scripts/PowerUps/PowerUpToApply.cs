﻿// Author: Pawel Granda
// Class that is applying power up on car according to the power up type (if the power up is of type speedDebuff, power up is automatically activated)

using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.PowerUps
{
    [RequireComponent(typeof(BoxCollider))]
    public class PowerUpToApply : NetworkBehaviour
    {
        [SerializeField]
        private bool speedBuff = false;

        [SerializeField]
        private bool speedDebuff = false;

        [SerializeField]
        private bool missile = false;

        [SerializeField]
        private bool oilSpill = false;

        private LevelController levelController;

        void Start()
        {
            levelController = GameController.Instance.LevelController;
        }
    
        void OnTriggerEnter(Collider other)
        {
            Player player = null;

            if (other.gameObject.name == levelController.LocalPlayer.name)
            {
                player = levelController.LocalPlayer;
            }
            else
            {
                Destroy(gameObject);
                return;
            }

            if (PowerUpActivator.Instance.PowerUp != null)
            {
                Destroy(gameObject);
                return;
            }

            if (speedBuff)
            {
                PowerUpActivator.Instance.PowerUp = new PowerUpSpeed(player);
            }
            else if (speedDebuff)
            {
                var powerUp = new PowerUpSlow(player);
                powerUp.ActivatePowerUp();
            }
            else if (missile)
            {
                PowerUpActivator.Instance.PowerUp = new PowerUpMissile(player);
            }
            else if (oilSpill)
            {
                PowerUpActivator.Instance.PowerUp = new PowerUpOil(player);
            }

            Destroy(gameObject);
        }
    }
}
