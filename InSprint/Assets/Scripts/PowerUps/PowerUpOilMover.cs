﻿// Author: Pawel Granda
// Script that is moving and handling collision on oil spill
using System.Linq;
using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.PowerUps
{
    public class PowerUpOilMover : NetworkBehaviour
    {
        private LevelController levelController;

        private Car localPlayerCar;

        void Start()
        {
            levelController = GameController.Instance.LevelController;
            localPlayerCar = levelController.LocalPlayer.GetComponent<Car>();
        }

        void Update()
        {
            UpdateMovement();
        }

        void OnTriggerEnter(Collider other)
        {

            var element = levelController.spawnedOils.FirstOrDefault(el => el.Key.Equals(gameObject.GetInstanceID()));
            if (element.Value.Equals(other.gameObject.GetComponent<Player>().name))
            {
                return;
            }

            if (!element.Value.Equals(levelController.LocalPlayer.name))
            {
                levelController.LocalPlayer.gameObject.GetComponent<Car>().Slowdown = true;
            }

            Destroy(gameObject);
            levelController.spawnedOils.Remove(gameObject.GetInstanceID());
        }

        private void UpdateMovement()
        {
            float zAxisVelocity = -localPlayerCar.CurrentVelocity * Time.deltaTime;
            transform.position += new Vector3(0f, 0f, zAxisVelocity);

            if (transform.position.z < -10f)
            {
                levelController.spawnedOils.Remove(gameObject.GetInstanceID());
                Destroy(gameObject);
            }
        }
    }
}