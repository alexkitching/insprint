﻿// Author: Pawel Granda
// Script that is spawning Power Ups (uncollected power ups shown as boxes)
using System.Collections;
using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.PowerUps
{
    public class PowerUpSpawner : NetworkBehaviour
    {
        [SerializeField]
        private Transform spawnPointLeft;

        [SerializeField]
        private Transform spawnPointCenter;

        [SerializeField]
        private Transform spawnPointRight;

        [SerializeField]
        private GameObject speedBuffPrefab;

        [SerializeField]
        private GameObject speedDebuffPrefab;

        [SerializeField]
        private GameObject missilePrefab;

        [SerializeField]
        private GameObject oilSpillPrefab;

        public static bool CanSpawn { get; private set; }

        private LevelController levelController;

        private void Start()
        {
            levelController = GameController.Instance.LevelController;
            StartCoroutine(WaitForFewSeconds());
        }

        private void Update()
        {
            if (!isServer || levelController.Opponent == null || !CanSpawn)
                return;

            CmdSpawn();
        }

        private void CmdSpawn()
        {
            CanSpawn = false;

            Transform spawnPoint = null;

            //Single line of powerups
            for (int i = 0; i < 3; i++)
            {
                int powerUpRandomizer = Random.Range(0, 4);
                switch (i)
                {
                    case 0:
                        //SpawnPointLeft           
                        spawnPoint = spawnPointLeft;
                        break;
                    case 1:
                        //SpawnPointCenter
                        spawnPoint = spawnPointCenter;
                        break;
                    case 2:
                        //SpawnPointRight
                        spawnPoint = spawnPointRight;
                        break;
                    default:
                        break;
                }

                RpcSpawnPowerup(spawnPoint.position, powerUpRandomizer);
            }

            StartCoroutine(WaitForFewSeconds());
        }

        [ClientRpc]
        private void RpcSpawnPowerup(Vector3 spawnPosition, int powerUpType)
        {
            Vector3 spawnPointPosition = spawnPosition;

            if (levelController != null && levelController.Opponent != null)
            {
                float distanceBetweenPlayers = levelController.LocalPlayer.GetComponent<LocalMovement>().OpponentDistance;
                if (distanceBetweenPlayers > 0)
                {
                    spawnPointPosition += new Vector3(0, 0, distanceBetweenPlayers);
                }
            }

            switch (powerUpType)
            {
                case 0:
                    Instantiate(speedBuffPrefab, spawnPointPosition, Quaternion.identity);
                    break;
                case 1:
                    Instantiate(speedDebuffPrefab, spawnPointPosition, Quaternion.identity);
                    break;
                case 2:
                    Instantiate(missilePrefab, spawnPointPosition, Quaternion.identity);
                    break;
                case 3:
                    Instantiate(oilSpillPrefab, spawnPointPosition, Quaternion.identity);
                    break;
                default:
                    break;
            }
        }

        IEnumerator WaitForFewSeconds()
        {
            yield return new WaitForSeconds(Random.Range(10, 15));
            CanSpawn = true;
        }
    }
}
