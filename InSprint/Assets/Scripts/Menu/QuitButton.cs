﻿// Author: Pawel Granda
// Script that kills the application after clicking the button
using UnityEngine;

namespace Assets.Scripts.Menu
{
    public class QuitButton : MonoBehaviour
    {
        public void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }

    }
}
