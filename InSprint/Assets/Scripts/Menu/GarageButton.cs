﻿// Author: Pawel Granda
// Script that changes the scene after clicking button
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Menu
{
    public class GarageButton : MonoBehaviour
    {
        public void GoToGarage()
        {
            SceneManager.LoadScene(3);
        }

    }
}
