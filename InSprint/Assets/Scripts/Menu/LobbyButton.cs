﻿// Author: Pawel Granda
// Script that changes the scene after clicking button
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Menu
{
    public class LobbyButton : MonoBehaviour
    {
        public void GoToLobby()
        {
            SceneManager.LoadScene(2);
        }
    }
}
