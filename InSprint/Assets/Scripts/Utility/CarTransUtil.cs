﻿// Author: Alex Kitching
// Static Class Responsible various Car Transform Utility Functions
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public static class CarTransUtil
    {
        // Recursive Method which Assigns Transform and Children to Shader
        public static void SetTransformAndChildMaterialsToShader(Transform a_transform, Shader a_shader)
        {
            if (a_transform.childCount > 0)
            {
                foreach (Transform child in a_transform)
                {
                    SetTransformAndChildMaterialsToShader(child, a_shader);
                }
            }

            MeshRenderer renderer = a_transform.GetComponent<MeshRenderer>();
            if (!renderer) return;
            foreach (Material material in renderer.materials)
            {
                material.shader = a_shader;
            }
        }

        // Recursive Method which deletes any Mesh Colliders on Transform and Children
        public static void DeleteMeshesOnTransformandChild(Transform a_transform)
        {
            if (a_transform.childCount > 0)
            {
                foreach (Transform transform in a_transform)
                {
                    DeleteMeshesOnTransformandChild(transform);
                }
            }

            MeshCollider collider = a_transform.GetComponent<MeshCollider>();
            if(collider)
                Object.Destroy(collider);
        }

        // Recursive Method which assigns a layer to Transform and Children
        public static void AssignLayerOnTransformAndChild(Transform a_transform, string a_layer)
        {
            if (a_transform.childCount > 0)
            {
                foreach (Transform transform in a_transform)
                {
                    AssignLayerOnTransformAndChild(transform, a_layer);
                }
            }

            a_transform.gameObject.layer = LayerMask.NameToLayer(a_layer);
        }
    }
}
