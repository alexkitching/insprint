﻿// Author: Alex Kitching
// Static Class Responsible various Math Utility Functions
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public static class MathUtil
    {
        // Calculates Modulus of two integers
        public static int Modulus(int x, int m)
        {
            return (x % m + m) % m;
        }

        // Returns rotation towards a target position
        public static Quaternion RotateTowards(Vector3 a_targetPos, Transform a_transform, float a_rotationSpeed)
        {
            Vector3 Dir = (a_targetPos - a_transform.position).normalized;
            return RotateTowards(Quaternion.LookRotation(Dir), a_transform.rotation, a_rotationSpeed);
        }

        // Returns rotation towards a target rotation
        public static Quaternion RotateTowards(Quaternion a_targetRot, Quaternion a_currentRot, float a_rotationSpeed)
        {
            return Quaternion.RotateTowards(a_currentRot, a_targetRot,  a_rotationSpeed);
        }

        // Returns returns a direction from an angle
        public static Vector3 DirFromAngle(float a_degrees, bool a_isGlobal, Transform a_transform)
        {
            if(!a_isGlobal)
            {
                a_degrees += a_transform.eulerAngles.y;
            }
            return new Vector3(Mathf.Sin(a_degrees * Mathf.Deg2Rad), 0, Mathf.Cos(a_degrees * Mathf.Deg2Rad));
        }

    }
}