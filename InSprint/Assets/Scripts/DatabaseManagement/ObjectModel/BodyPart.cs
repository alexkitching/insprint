﻿// Author: Pawel Granda
// Class of object model that represents BodyPart database table
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class BodyPart : CarPart
    {
        [PrimaryKey, AutoIncrement, NotNull]
        public int BodyPartID { get; set; }

        public string ToString()
        {
            return string.Format("BodyPartID: {0}, Name: {1}, PrefabPath: {2}, Cost: {3}, Valid: {4}", BodyPartID, Name, PrefabPath, Cost, Valid);
        }
    }
}