﻿// Author: Pawel Granda
// Class of object model that represents Loadout database table
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class Loadout : ObjectWithValidity
    {
        [PrimaryKey, AutoIncrement, NotNull]
        public int LoadoutID { get; set; }

        [NotNull]
        public string Name { get; set; }

        [Indexed, NotNull]
        public int PlayerID { get; set; }

        [Indexed, NotNull]
        public int BodyPartID { get; set; }

        [Indexed, NotNull]
        public int ChassisPartID { get; set; }

        [Indexed, NotNull]
        public int WheelPartID { get; set; }

        public string ToString()
        {
            return string.Format("LoadoutID: {0}, Name: {1}, PlayerID: {2}, BodyPartID: {3}, ChassisPartID: {4}, WheelPartID: {5}",LoadoutID, Name, PlayerID, BodyPartID, ChassisPartID, WheelPartID);
        }
    }
}