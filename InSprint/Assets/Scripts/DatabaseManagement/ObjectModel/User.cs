﻿// Author: Pawel Granda
// Class of object model that represents User database table
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;
using SQLite4Unity3d;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class User : ObjectWithValidity
    {
        [PrimaryKey, AutoIncrement, NotNull]
        public int UserID { get; set; }

        [NotNull]
        public string Username { get; set; }

        [NotNull]
        public string Password { get; set; }

        [NotNull]
        public string Email { get; set; }

        [NotNull]
        public string CreatedTimeStamp { get; set; }

        public string ToString()
        {
            return string.Format("UserID: {0}, Username: {1}, Password: {2}, Email: {3}, CreatedTimeStamp: {4}", UserID, Username, Password, Email, CreatedTimeStamp);
        }
    }
}