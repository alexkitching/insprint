﻿// Author: Pawel Granda
// Class of object model that represents PlayerBodyPart database table
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class PlayerBodyPart
    {
        [Indexed, NotNull]
        public int PlayerID { get; set; }

        [Indexed, NotNull]
        public int BodyPartID { get; set; }

        public PlayerBodyPart()
        {

        }

        public PlayerBodyPart(int playerId, int bodyPartId)
        {
            PlayerID = playerId;
            BodyPartID = bodyPartId;
        }

        public string ToString()
        {
            return string.Format("PlayerID: {0}, BodyPartID: {1}", PlayerID, BodyPartID);
        }
    }
}