﻿// Author: Pawel Granda
// Class of object model that represents PlayerWheelPart database table
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class PlayerWheelPart
    {
        [Indexed, NotNull]
        public int PlayerID { get; set; }

        [Indexed, NotNull]
        public int WheelPartID { get; set; }

        public PlayerWheelPart()
        {

        }

        public PlayerWheelPart(int playerId, int wheelPartId)
        {
            PlayerID = playerId;
            WheelPartID = wheelPartId;
        }

        public string ToString()
        {
            return string.Format("PlayerID: {0}, WheelPartID: {1}", PlayerID, WheelPartID);
        }
    }
}
