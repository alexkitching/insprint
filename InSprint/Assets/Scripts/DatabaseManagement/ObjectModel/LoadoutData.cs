﻿// Author: Pawel Granda
// Class of object model that contains full Loadout data (it contains BodyPart, ChassisPart and WheelPart objects, when Loadout only contains their IDs)
namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class LoadoutData : ObjectWithValidity
    {
        public LoadoutData(Loadout loadout, BodyPart bodyPart, ChassisPart chassisPart, WheelPart wheelPart)
        {
            LoadoutID = loadout.LoadoutID;
            Name = loadout.Name;
            PlayerID = loadout.PlayerID;
            BodyPart = bodyPart;
            ChassisPart = chassisPart;
            WheelPart = wheelPart;
        }

        public int LoadoutID { get; set; }

        public string Name { get; set; }

        public int PlayerID { get; set; }

        public BodyPart BodyPart { get; set; }

        public ChassisPart ChassisPart { get; set; }

        public WheelPart WheelPart { get; set; }

        public string ToString()
        {
            return string.Format("LoadoutID: {0}, Name: {1}, PlayerID: {2} \n {3} \n {4} \n {5} ", LoadoutID, Name, PlayerID, BodyPart.ToString(), ChassisPart.ToString(), WheelPart.ToString());
        }
    }
}