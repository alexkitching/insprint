﻿// Author: Pawel Granda
// Class of object model that represents ChassisPart database table
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class ChassisPart : CarPart
    {
        [PrimaryKey, AutoIncrement, NotNull]
        public int ChassisPartID { get; set; }

        [NotNull]
        public int WheelCount { get; set; }

        public string ToString()
        {
            return string.Format("ChassisPartID: {0}, Name: {1}, PrefabPath: {2}, Cost: {3}, WheelCount: {4}, Valid: {5}", ChassisPartID, Name, PrefabPath, Cost, WheelCount, Valid);
        }
    }
}