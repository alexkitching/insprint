﻿// Author: Pawel Granda
// Class of object model that represents Player database table
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;
using SQLite4Unity3d;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class Player : ObjectWithValidity
    {
        [PrimaryKey, AutoIncrement, NotNull]
        public int PlayerID { get; set; }

        [Indexed, NotNull]
        public int UserID { get; set; }

        [NotNull]
        public int Coins { get; set; }

        public string ToString()
        {
            return string.Format("PlayerID: {0}, UserID: {1}, Coins: {2}", PlayerID, UserID, Coins);
        }
    }
}