﻿// Author: Pawel Granda
// Class of object model that represents PlayerChassisPart database table
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class PlayerChassisPart
    {
        [Indexed, NotNull]
        public int PlayerID { get; set; }

        [Indexed, NotNull]
        public int ChassisPartID { get; set; }

        public PlayerChassisPart()
        {

        }

        public PlayerChassisPart(int playerId, int chassisPartId)
        {
            PlayerID = playerId;
            ChassisPartID = chassisPartId;
        }

        public string ToString()
        {
            return string.Format("PlayerID: {0}, ChassisPartID: {1}", PlayerID, ChassisPartID);
        }
    }
}