﻿// Author: Pawel Granda
// Base class containing general information common for all car parts.
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class CarPart : ObjectWithValidity
    {
        [NotNull, Unique]
        public string Name { get; set; }

        [NotNull]
        public string PrefabPath { get; set; }

        [NotNull]
        public int Cost { get; set; }
    }
}