﻿// Author: Pawel Granda
// Class of object model that represents Wheelpart database table
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public class WheelPart : CarPart
    {
        [PrimaryKey, AutoIncrement, NotNull]
        public int WheelPartID { get; set; }

        public string ToString()
        {
            return string.Format("WheelPartID: {0}, Name: {1}, PrefabPath: {2}, Cost: {3}, Valid: {4}", WheelPartID, Name, PrefabPath, Cost, Valid);
        }
    }
}