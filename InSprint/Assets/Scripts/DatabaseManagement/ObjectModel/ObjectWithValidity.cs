﻿// Author: Pawel Granda
// Base class containing valid property common for all object model classes
using Assets.Scripts.DatabaseManagement.SQLite.Attributes;
using SQLite4Unity3d;

namespace Assets.Scripts.DatabaseManagement.ObjectModel
{
    public abstract class ObjectWithValidity
    {
        [NotNull]
        public int Valid { get; set; }
    }
}