﻿// Script used to hash the string that makes the password
using System.Security.Cryptography;
using System.Text;

namespace Assets.Scripts.DatabaseManagement
{
    internal static class HashHelper
    {
        private static byte[] GetHash(string inputString)
        {
            var algorithm = MD5.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            var stringBuilder = new StringBuilder();
            foreach (byte element in GetHash(inputString))
            {
                stringBuilder.Append(element.ToString("X2"));
            }

            return stringBuilder.ToString();
        }
    }
}
