﻿// Author: Pawel Granda (Examples and explanation) & Alex Kitching (Game implementation)
using System.Collections.Generic;
using Assets.Scripts.CarCustomisation;
using Assets.Scripts.DatabaseManagement.ObjectModel;
using UnityEngine;

// This script is for managing all what happens with creating, assigning, updating, invalidating and returning all CarParts data.
// To properly take advantage of all the good that database gives you, simply create a function of your need, and execute method on carPartsRepository object.
// If you need to use Username of a Player you can call AuthenticationManager.Instance.LoggedPlayerName to return it.
// If you wish to print out any data to the console. You can simply call Debug.Log() and inside it execute the method that returns the data 
// and call ToString() function that I have implemented for you.
// For your convenience I have create simple GenerateTestData function with some of the methods, so you can have a look how to use them.

namespace Assets.Scripts.DatabaseManagement
{
    public class CarPartsManager : MonoBehaviour
    {
        private CarPartsRepository carPartsRepository;

        private Loadout currentLoadout;

        public CarPartList partList;

        public int CurrentPlayerID = -1;

        void Awake()
        {
            carPartsRepository = new CarPartsRepository();

            bool Cleared = false;

            // Uncomment to force a reset of part database
            //Cleared = true;
            //carPartsRepository.ClearTable("BodyPart");
            //carPartsRepository.ClearTable("ChassisPart");
            //carPartsRepository.ClearTable("WheelPart");

            if (partList != null && !Cleared) // Partlist present and database not cleared
            {
                EnumerateCarParts(); // Check Database is up to date
            }

            IList<Loadout> _list;

            if (AuthenticationManager.Instance != null) // Authentication Present
            {
                // Get Current Player ID and Loadouts 
                CurrentPlayerID = AuthenticationManager.Instance.LoggedPlayerID;
                _list = carPartsRepository.GetAllLoadoutsForPlayer(CurrentPlayerID);

                if(!carPartsRepository.PlayerHasDefaults(CurrentPlayerID)) // if Player hasn't got default parts assigned, assign them
                    carPartsRepository.AssignDefaultsToPlayer(CurrentPlayerID);
            }
            else // Get all loadouts from database
            {
                _list = carPartsRepository.GetAllLoadouts();
            }

            int count = _list.Count;

            if (count > 0) // Loadouts Present
            {
                foreach (Loadout item in _list) // Ensure only one loadout is valid
                {
                    if (item == _list[count - 1]) continue;
                    item.Valid = 0;
                    carPartsRepository.UpdateLoadout(item);
                }

                // Assign Current Loadout
                currentLoadout = _list[count - 1];
            }
            else // No Loadout Present assign default blank loadout
            {
                Loadout loadout = new Loadout {BodyPartID = 0, ChassisPartID = 0, Name = "My Car", PlayerID = CurrentPlayerID, Valid = 1, WheelPartID = 0};
                carPartsRepository.AssignChassisPartToPlayer(CurrentPlayerID, 0);
                
                carPartsRepository.SaveLoadout(loadout);
                currentLoadout = loadout;
            }
        }

        // Returns Current Loadout
        public CarLoadout GetCurrentLoadout()
        {
            return new CarLoadout(currentLoadout.ChassisPartID, currentLoadout.BodyPartID, currentLoadout.WheelPartID, currentLoadout.Name);
        }

        // Saves Loadout to Database
        public void SaveLoadout(CarLoadout a_loadout)
        {
            currentLoadout.BodyPartID = a_loadout.DriverId;
            currentLoadout.ChassisPartID = a_loadout.ChassisId;
            currentLoadout.WheelPartID = a_loadout.WheelId;
            currentLoadout.Name = a_loadout.Name;

            carPartsRepository.UpdateLoadout(currentLoadout);
            Debug.Log("Loadout Saved");
        }

        // Compares Scriptable Object Part Lists with those stored in the database,
        // any missing parts will be added and any parts which values dont match are updated accordingly.
        private void EnumerateCarParts()
        {
            int i;
            // Get list of Chassis
            var chassisList = carPartsRepository.GetAllChassisParts();
            for (i = 0; i < partList.Chassis.Length; ++i) // Loop through each Part
            {
                Chassis currentChassis = partList.Chassis[i];

                if (i + 1 > chassisList.Count) // Part not present in database, new chassis is created to be added
                {
                    ChassisPart chassisPart = new ChassisPart
                    {
                        WheelCount    = currentChassis.WheelCount,
                        Name          = currentChassis.Name,
                        PrefabPath    = "",
                        Cost          = currentChassis.Cost,
                        Valid         = 1
                    };

                    Debug.Log("Chassis Added To Database!");
                    carPartsRepository.SaveChassisPart(chassisPart);
                }
                else // Part is compared with existing database record
                {
                    ChassisPart chassisRecord = chassisList[i];
                    bool correct = true;

                    if (chassisRecord.WheelCount != currentChassis.WheelCount)
                    {
                        correct = false;
                        chassisRecord.WheelCount = currentChassis.WheelCount;
                    }

                    if (chassisRecord.Cost != currentChassis.Cost)
                    {
                        correct = false;
                        chassisRecord.Cost = currentChassis.Cost;
                    }

                    if (chassisRecord.Name != currentChassis.Name)
                    {
                        correct = false;
                        chassisRecord.Name = currentChassis.Name;
                    }

                    if (correct) // Values match
                        continue;

                    // Updates record if values changed
                    carPartsRepository.UpdateChassisPart(chassisRecord);
                    Debug.Log("Chassis Updated");
                }
            }

            var driverList = carPartsRepository.GetAllBodyParts();
            for (i = 0; i < partList.Drivers.Length; ++i) // Loop through each Part
            {
                Driver currentDriver = partList.Drivers[i];
                
                if (i + 1 > driverList.Count) // Part not present in database, new chassis is created to be added
                {
                    BodyPart bodyPart = new BodyPart
                    {
                        Name          = currentDriver.Name,
                        PrefabPath    = "",
                        Cost          = currentDriver.Cost,
                        Valid         = 1
                    };

                    Debug.Log("Driver Added To Database!");
                    carPartsRepository.SaveBodyPart(bodyPart);
                }
                else // Part is compared with existing database record
                {
                    BodyPart driverRecord = driverList[i];
                    bool correct = true;

                    if (driverRecord.Cost != currentDriver.Cost)
                    {
                        correct = false;
                        driverRecord.Cost = currentDriver.Cost;
                    }

                    if (driverRecord.Name != currentDriver.Name)
                    {
                        correct = false;
                        driverRecord.Name = currentDriver.Name;
                    }

                    if (correct) // Values match
                        continue;

                    // Updates record if values changed
                    carPartsRepository.UpdateBodyPart(driverRecord);
                    Debug.Log("Driver Updated");
                }
            }

            var wheelList = carPartsRepository.GetAllWheelParts();
            for (i = 0; i < partList.Wheels.Length; ++i) // Loop through each Part
            {
                Wheel currentWheel = partList.Wheels[i];

                if (i + 1 > wheelList.Count) // Part not present in database, new chassis is created to be added
                {
                    WheelPart wheelPart = new WheelPart
                    {
                        Name = currentWheel.Name,
                        PrefabPath = "",
                        Cost = currentWheel.Cost,
                        Valid = 1
                    };

                    Debug.Log("Wheel Added To Database!");
                    carPartsRepository.SaveWheelPart(wheelPart);
                }
                else  // Part is compared with existing database record
                {
                    WheelPart wheelRecord = wheelList[i];
                    bool correct = true;

                    if (wheelRecord.Cost != currentWheel.Cost)
                    {
                        correct = false;
                        wheelRecord.Cost = currentWheel.Cost;
                    }

                    if (wheelRecord.Name != currentWheel.Name)
                    {
                        correct = false;
                        wheelRecord.Name = currentWheel.Name;
                    }

                    if (correct) // Values match
                        continue;

                    // Updates record if values changed
                    carPartsRepository.UpdateWheelPart(wheelRecord);
                    Debug.Log("Wheel Updated");
                }
            }
        }
    }
}
