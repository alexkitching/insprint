﻿// Author: Pawel Granda
// Class responsible for establishing connection with the database in the file
using System;
using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using Assets.Scripts.DatabaseManagement.SQLite;

namespace Assets.Scripts.DatabaseManagement
{
	public class DatabaseManager
	{
		public SQLiteConnection Connection { get; private set; }

		private static DatabaseManager instance;

		private static object syncRoot = new object();

		private const string databaseName = "InSprintDatabase.db";

		public static DatabaseManager Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
						{
							instance = new DatabaseManager();
						}
					}
				}
				return instance;
			}
		}

		private DatabaseManager()
		{
			string databasePath = string.Empty;

#if UNITY_EDITOR

			databasePath = string.Format(@"Assets/StreamingAssets/{0}", databaseName);

#elif UNITY_ANDROID

			databasePath = string.Format("{0}/{1}", Application.persistentDataPath, databaseName);
			LoadTheDatabaseFromStreamingAssetsToGivenPath(databasePath);

#else
			databasePath = Application.dataPath + "/StreamingAssets/" + databaseName;
#endif

			Connection = new SQLiteConnection(databasePath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
		}

		private void LoadTheDatabaseFromStreamingAssetsToGivenPath(string databasePath)
		{
			if (File.Exists(databasePath))
			{
				return;
			}

			var androidStreamingAssetsPath = new WWW(string.Format(@"jar:file://{0}!/assets/{1}", Application.dataPath, databaseName));
			while (!androidStreamingAssetsPath.isDone)
			{

			}

			File.WriteAllBytes(databasePath, androidStreamingAssetsPath.bytes);
		}
	}
}