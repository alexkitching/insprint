﻿// Author: Pawel Granda
// Class creating data access layer, responsible for quering and updating coins data
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.DatabaseManagement.ObjectModel;
using UnityEngine;

namespace Assets.Scripts.DatabaseManagement
{
    public class CoinsRepository
    {
        private DatabaseManager databaseManager;

        public CoinsRepository()
        {
            databaseManager = DatabaseManager.Instance;
        }

        public int GetPlayerCoins(string username)
        {
            var user = databaseManager.Connection.Table<User>().FirstOrDefault(u => u.Username.Equals(username) && u.Valid == 1);
            if (user == null)
            {
                return 0;
            }

            var player = databaseManager.Connection.Table<ObjectModel.Player>().FirstOrDefault(p => p.UserID == user.UserID && p.Valid == 1);

            return player.Coins;
        }

        public void UpdatePlayerCoins(string username, int coins)
        {
            var user = databaseManager.Connection.Table<User>().FirstOrDefault(u => u.Username.Equals(username) && u.Valid == 1);
            if (user == null)
            {
                return;
            }

            var player = databaseManager.Connection.Table<ObjectModel.Player>().FirstOrDefault(p => p.UserID == user.UserID && p.Valid == 1);
            player.Coins = coins;

            databaseManager.Connection.Update(player);
        }
    }
}
