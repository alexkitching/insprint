﻿// Author: Pawel Granda
// Class responsible for managing all coins operations

using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.DatabaseManagement
{
    public class CoinsManager : MonoBehaviour
    {
        [SerializeField]
        private static Text coinsText;

        private CoinsRepository coinsRepository;

        private bool coinsLoaded;

        void Awake()
        {
            coinsRepository = new CoinsRepository();

            coinsText = GameController.Instance.CoinsText;
        }

        void Update()
        {
            if (!coinsLoaded && GameController.Instance.LevelController.LocalPlayer != null)
            {
                LoadPlayerCoins();
                coinsLoaded = true;
            }
        }

        private void OnDestroy()
        {
            UpdatePlayerCoinsInDatabase();
        }

        public void LoadPlayerCoins()
        {
            var coins = coinsRepository.GetPlayerCoins(AuthenticationManager.Instance.LoggedPlayerName);
            GameController.Instance.LevelController.LocalPlayer.Coins = coins;
            coinsText.text = "Coins: " + coins; 
        }

        public static void UpdatePlayerCoins(int coins)
        {
            GameController.Instance.LevelController.LocalPlayer.Coins = coins;
            coinsText.text = "Coins: " + coins;
        }

        public void UpdatePlayerCoinsInDatabase()
        {
            coinsRepository.UpdatePlayerCoins(AuthenticationManager.Instance.LoggedPlayerName, GameController.Instance.LevelController.LocalPlayer.Coins);
        }
    }
}