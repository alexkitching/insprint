﻿// Author: Pawel Granda
// Class creating data access layer, responsible for quering and updating authentication data
using System;
using Assets.Scripts.DatabaseManagement.ObjectModel;

namespace Assets.Scripts.DatabaseManagement
{
    public class AuthenticationRepository
    {
        private DatabaseManager databaseManager;

        public AuthenticationRepository()
        {
            databaseManager = DatabaseManager.Instance;
        }

        public bool CheckIfUserAlreadyExists(string username, string email)
        {
            var user = databaseManager.Connection.Table<User>().FirstOrDefault(u => u.Username.Equals(username) && u.Email.Equals(email) && u.Valid == 1);

            if (user == null)
            {
                return false;
            }

            return true;
        }

        public void SaveUserAndCreatePlayer(string username, string email, string passwordHash)
        {
            var user = new User()
            {
                Username = username,
                Email = email,
                Password = passwordHash,
                CreatedTimeStamp = DateTime.Now.ToString(),
                Valid = 1
            };
            databaseManager.Connection.Insert(user);

            var player = new ObjectModel.Player()
            {
                UserID = user.UserID,
                Coins = 0,
                Valid = 1
            };

            CarPartsRepository repo = new CarPartsRepository();
            repo.AssignDefaultsToPlayer(user.UserID);

            databaseManager.Connection.Insert(player);
        }

        public int AuthorizeUser(string username, string passwordHash)
        {
            var user = databaseManager.Connection.Table<User>().FirstOrDefault(u => u.Username.Equals(username) && u.Password.Equals(passwordHash) && u.Valid == 1);
            if (user == null)
            {
                return 0;
            }

            var player = databaseManager.Connection.Table<ObjectModel.Player>().FirstOrDefault(p => p.UserID == user.UserID && p.Valid == 1);

            return player != null ? player.PlayerID : 0;
        }
    }
}

