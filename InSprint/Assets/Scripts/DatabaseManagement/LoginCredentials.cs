﻿// Author: Pawel Granda
// Class containing all data required by authentication process
namespace Assets.Scripts.DatabaseManagement
{
    public class LoginCredentials
    {
        public string Username;
        public string Password;
    }
}