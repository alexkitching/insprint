﻿// Author: Pawel Granda
// Class responsible for managing all authentication operations
using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Text;

namespace Assets.Scripts.DatabaseManagement
{
	public class AuthenticationManager : MonoBehaviour
	{
		public static AuthenticationManager Instance;

		[SerializeField]
		private GameObject loginParent;

		[SerializeField]
		private GameObject registerParent;

		[SerializeField]
		private InputField loginUsernameField;

		[SerializeField]
		private InputField loginPasswordField;

		[SerializeField]
		private InputField registerUsernameField;

		[SerializeField]
		private InputField registerPasswordField;

		[SerializeField]
		private InputField registerConfirmPasswordField;

		[SerializeField]
		private InputField registerEmailField;

		[SerializeField]
		private Text loginErrorText;

		[SerializeField]
		private Text registerErrorText;

		[SerializeField]
		private Toggle rememberMe;

		private LoginCredentials loginCredentials;

		private const string jsonFile = "RememberMe.json";

		public int LoggedPlayerID { get; private set; }
		public string LoggedPlayerName { get; private set; }
		private AuthenticationRepository authenticationRepository;

		void Awake()
		{
			Instance = this;
			DontDestroyOnLoad(this);
			ResetAllUIElements();
			authenticationRepository = new AuthenticationRepository();
			loginParent.gameObject.SetActive(true);
			registerParent.gameObject.SetActive(false);
			loginCredentials = new LoginCredentials();
			LoadUserCredentials();
		}

		void ResetAllUIElements()
		{
			loginUsernameField.text = null;
			loginPasswordField.text = null;
			registerUsernameField.text = null;
			registerPasswordField.text = null;
			registerConfirmPasswordField.text = null;
			registerEmailField.text = null;
			loginErrorText.text = null;
			registerErrorText.text = null;
			rememberMe.isOn = false;
		}

		public void DisplayRegistrationView()
		{
			ResetAllUIElements();
			loginParent.gameObject.SetActive(false);
			registerParent.gameObject.SetActive(true);
		}

		public void GoBackToLoginView()
		{
			ResetAllUIElements();
			loginParent.gameObject.SetActive(true);
			registerParent.gameObject.SetActive(false);
		}

		public void SaveUserAndCreatePlayer()
		{
			if (!ValidateRegistrationForm())
			{
				return;
			}

			var passwordHash = HashHelper.GetHashString(registerPasswordField.text);
			authenticationRepository.SaveUserAndCreatePlayer(registerUsernameField.text, registerEmailField.text, passwordHash);

			SceneManager.LoadScene(0);
		}

		private bool ValidateRegistrationForm()
		{
			if (!CheckIfAllRequiredFieldsAreFilled())
			{
				return false;
			}

			if(!CheckIfPasswordMatchRules())
			{
				return false;
			}

			if (!CheckIfPasswordsMatch())
			{
				return false;
			}

			if (CheckIfUserAlreadyExists())
			{
				return false;
			}

			if (!CheckEmailFormat())
			{
				return false;
			}

			return true;
		}

		private bool CheckIfPasswordMatchRules()
		{
			var result = Regex.IsMatch(registerPasswordField.text, @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$");

			if (!result)
			{
				registerErrorText.text = string.Format("Password must be at least 8 character length, contain 1 uppercase letter, 1 lowercase letter and 1 digit.");
			}

			return result;
		}

		private bool CheckIfAllRequiredFieldsAreFilled()
		{
			if (registerUsernameField.text.Equals(string.Empty) || registerPasswordField.text.Equals(string.Empty) ||
				registerConfirmPasswordField.text.Equals(string.Empty) || registerEmailField.text.Equals(string.Empty))
			{
				registerErrorText.text = string.Format("Fill all the fields!");
				return false;
			}

			return true;
		}

		private bool CheckIfPasswordsMatch()
		{
			if (registerPasswordField.text.Equals(registerConfirmPasswordField.text))
			{
				return true;
			}

			registerErrorText.text = string.Format("Passwords don't match!");
			return false;
		}

		private bool CheckIfUserAlreadyExists()
		{
			if (authenticationRepository.CheckIfUserAlreadyExists(registerUsernameField.text, registerEmailField.text))
			{
				registerErrorText.text = string.Format("User already exists!");
				return true;
			}

			return false;
		}

		public bool CheckEmailFormat()
		{
			if (String.IsNullOrEmpty(registerEmailField.text))
			{
				registerErrorText.text = string.Format("E-mail cannot be empty!");
				return false;
			}

			var result = Regex.IsMatch(registerEmailField.text,
				   @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
				   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
				   RegexOptions.IgnoreCase);

			if (!result)
			{
				registerErrorText.text = string.Format("E-mail format is not correct!");
			}

			return result;
		}

		public void LoginUser()
		{
			var passwordHash = HashHelper.GetHashString(loginPasswordField.text);

			if (loginUsernameField.text.Equals(string.Empty) || loginPasswordField.text.Equals(string.Empty))
			{
				loginErrorText.text = string.Format("Fill all the fields!");
				return;
			}

			int playerID = authenticationRepository.AuthorizeUser(loginUsernameField.text, passwordHash);

			if (playerID == 0)
			{
				loginErrorText.text = string.Format("Not correct username or password!");
				return;
			}

			LoggedPlayerID = playerID;
			LoggedPlayerName = loginUsernameField.text;

			SceneManager.LoadScene(1);
		}

		public void SaveUserCredentials()
		{
			if (!rememberMe.isOn)
			{
				return;
			}
			string jsonPath = String.Empty;

#if UNITY_EDITOR

			jsonPath = string.Format(@"Assets/StreamingAssets/{0}", jsonFile);

#elif UNITY_ANDROID

			jsonPath = string.Format("{0}/{1}", Application.persistentDataPath, jsonFile);
#else
			jsonPath = Application.dataPath + "/StreamingAssets/" + jsonFile;
#endif
			loginCredentials.Username = loginUsernameField.text;
			loginCredentials.Password = loginPasswordField.text;

			string jsonData = JsonUtility.ToJson(loginCredentials, true);

			File.WriteAllText(jsonPath, jsonData);
		}

		public void LoadUserCredentials()
		{
			string jsonPath = String.Empty;

#if UNITY_EDITOR

			jsonPath = string.Format(@"Assets/StreamingAssets/{0}", jsonFile);

#elif UNITY_ANDROID

			jsonPath = string.Format("{0}/{1}", Application.persistentDataPath, jsonFile);
#else
			jsonPath = Application.dataPath + "/StreamingAssets/" + jsonFile;
#endif
			loginCredentials =
				JsonUtility.FromJson<LoginCredentials>(
					File.ReadAllText(jsonPath));
			loginUsernameField.text = loginCredentials.Username;
			loginPasswordField.text = loginCredentials.Password;
		}
	}
}

  