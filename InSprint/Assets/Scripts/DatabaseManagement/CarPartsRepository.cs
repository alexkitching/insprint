﻿// Author: Pawel Granda
// Class creating data access layer, responsible for quering and updating car parts data
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using Assets.Scripts.DatabaseManagement.ObjectModel;

namespace Assets.Scripts.DatabaseManagement
{
    public class CarPartsRepository
    {
        private readonly DatabaseManager databaseManager;

        public CarPartsRepository()
        {
            databaseManager = DatabaseManager.Instance;
        }

        // Return list of all WheelParts in the database.
        public IList<WheelPart> GetAllWheelParts()
        {
            return databaseManager.Connection.Table<WheelPart>().Where(wp => wp.Valid == 1).ToList();
        }

        // Return list of all WheelParts that have the same IDs as in List.
        public IList<WheelPart> GetWheelPartsByIds(IList<int> ids)
        {
            return databaseManager.Connection.Table<WheelPart>().Where(wp => ids.Contains(wp.WheelPartID) && wp.Valid == 1).ToList();
        }

        // Return list of all BodyParts (driver) in the database.
        public IList<BodyPart> GetAllBodyParts()
        {
            return databaseManager.Connection.Table<BodyPart>().Where(bp => bp.Valid == 1).ToList();
        }

        // Return list of all BodyParts (driver) that have the same IDs as in List.
        public IList<BodyPart> GetBodyPartsByIds(IList<int> ids)
        {
            return databaseManager.Connection.Table<BodyPart>().Where(bp => ids.Contains(bp.BodyPartID) && bp.Valid == 1).ToList();
        }

        // Return list of all ChassisParts in the database.
        public IList<ChassisPart> GetAllChassisParts()
        {
            return databaseManager.Connection.Table<ChassisPart>().Where(cp => cp.Valid == 1).ToList();
        }

        // Return list of all ChassisParts that have the same IDs as in List.
        public IList<ChassisPart> GetChassisPartsByIds(IList<int> ids)
        {
            return databaseManager.Connection.Table<ChassisPart>().Where(cp => ids.Contains(cp.ChassisPartID) && cp.Valid == 1).ToList();
        }

        // Return list of all Loadouts in the database. This data type indicates that returned object will contain only IDs to the Car Parts inside.
        public IList<Loadout> GetAllLoadouts()
        {
            return databaseManager.Connection.Table<Loadout>().Where(l => l.Valid == 1).ToList();
        }

        // Return list of all Loadouts owned by given Player. This data type indicates that returned object will contain Car Parts as objects.
        public IList<LoadoutData> GetAllLoadoutsDataForPlayer(string username)
        {
            var player = GetPlayerByUsername(username);

            return GetAllLoadoutsDataForPlayer(player.PlayerID);
        }

        // Return list of all Loadouts owned by given Player. This data type indicates that returned object will contain only IDs to the Car Parts inside.
        public IList<Loadout> GetAllLoadoutsForPlayer(string username)
        {
            var player = GetPlayerByUsername(username);

            return GetAllLoadoutsForPlayer(player.PlayerID);
        }

        // Return list of all Loadouts owned by given Player. This data type indicates that returned object will contain Car Parts as objects.
        public IList<LoadoutData> GetAllLoadoutsDataForPlayer(int playerId)
        {
            var loadouts = databaseManager.Connection.Table<Loadout>().Where(l => l.PlayerID == playerId && l.Valid == 1).ToList();
            var bodyParts = GetBodyPartsByIds(loadouts.Select(l => l.BodyPartID).ToList());
            var wheelParts = GetWheelPartsByIds(loadouts.Select(l => l.WheelPartID).ToList());
            var chassisParts = GetChassisPartsByIds(loadouts.Select(l => l.ChassisPartID).ToList());

            IList<LoadoutData> loadoutDatas = new List<LoadoutData>();
            foreach (var loadout in loadouts)
            {
                loadoutDatas.Add(new LoadoutData(loadout,
                    bodyParts.FirstOrDefault(bp => bp.BodyPartID == loadout.BodyPartID),
                    chassisParts.FirstOrDefault(cp => cp.ChassisPartID == loadout.ChassisPartID),
                    wheelParts.FirstOrDefault(wp => wp.WheelPartID == loadout.WheelPartID)));
            }

            return loadoutDatas;
        }

        // Return list of all Loadouts owned by given Player. This data type indicates that returned object will contain only IDs to the Car Parts inside.
        public IList<Loadout> GetAllLoadoutsForPlayer(int playerId)
        {
            return databaseManager.Connection.Table<Loadout>().Where(l => l.PlayerID == playerId && l.Valid == 1).ToList();
        }

        // Return a single WheelPart of given ID.
        public WheelPart GetWheelPartById(int id)
        {
            return databaseManager.Connection.Table<WheelPart>().FirstOrDefault(wp => wp.WheelPartID == id && wp.Valid == 1);
        }

        // Return a single WheelPart of given name.
        public WheelPart GetWheelPartByName(string name)
        {
            return databaseManager.Connection.Table<WheelPart>().FirstOrDefault(wp => wp.Name == name && wp.Valid == 1);
        }

        // Return a single BodyPart of given ID.
        public BodyPart GetBodyPartById(int id)
        {
            return databaseManager.Connection.Table<BodyPart>().FirstOrDefault(bp => bp.BodyPartID == id && bp.Valid == 1);
        }

        // Return a single BodyPart of given name.
        public BodyPart GetBodyPartByName(string name)
        {
            return databaseManager.Connection.Table<BodyPart>().FirstOrDefault(bp => bp.Name == name && bp.Valid == 1);
        }

        // Return a single ChassisPart of given ID.
        public ChassisPart GetChassisPartById(int id)
        {
            return databaseManager.Connection.Table<ChassisPart>().FirstOrDefault(cp => cp.ChassisPartID == id && cp.Valid == 1);
        }

        // Return a single ChassisPart of given name.
        public ChassisPart GetChassisPartByName(string name)
        {
            return databaseManager.Connection.Table<ChassisPart>().FirstOrDefault(cp => cp.Name == name && cp.Valid == 1);
        }

        // Return a single Loadout of given ID.
        public Loadout GetLoadoutById(int id)
        {
            return databaseManager.Connection.Table<Loadout>().FirstOrDefault(l => l.LoadoutID == id && l.Valid == 1);
        }

        // Return a single Loadout of given name.
        public Loadout GetLoadoutByName(string name)
        {
            return databaseManager.Connection.Table<Loadout>().FirstOrDefault(l => l.Name == name && l.Valid == 1);
        }

        // Save WheelPart to the database.
        public void SaveWheelPart(string name, string prefabPath, int cost)
        {
            var wheelPart = new WheelPart()
            {
                Name = name,
                PrefabPath = prefabPath,
                Cost = cost,
                Valid = 1
            };
            databaseManager.Connection.Insert(wheelPart);
        }

        // Save WheelPart to the database.
        public void SaveWheelPart(WheelPart wheelPart)
        {
            databaseManager.Connection.Insert(wheelPart);
        }

        // Update WheelPart in the database.
        public void UpdateWheelPart(int id, string name, string prefabPath, int cost)
        {
            var wheelPart = GetWheelPartById(id);
            wheelPart.Name = name;
            wheelPart.PrefabPath = prefabPath;
            wheelPart.Cost = cost;
            databaseManager.Connection.Update(wheelPart);
        }

        // Update WheelPart in the database.
        public void UpdateWheelPart(WheelPart wheelPart)
        {
            databaseManager.Connection.Update(wheelPart);
        }

        // Save BodyPart to the database.
        public void SaveBodyPart(string name, string prefabPath, int cost)
        {
            var bodyPart = new BodyPart()
            {
                Name = name,
                PrefabPath = prefabPath,
                Cost = cost,
                Valid = 1
            };
            databaseManager.Connection.Insert(bodyPart);
        }

        // Save BodyPart to the database.
        public void SaveBodyPart(BodyPart bodyPart)
        {
            databaseManager.Connection.Insert(bodyPart);
        }

        // Update BodyPart in the database.
        public void UpdateBodyPart(int id, string name, string prefabPath, int cost)
        {
            var bodyPart = GetBodyPartById(id);
            bodyPart.Name = name;
            bodyPart.PrefabPath = prefabPath;
            bodyPart.Cost = cost;
            databaseManager.Connection.Update(bodyPart);
        }

        // Update BodyPart in the database.
        public void UpdateBodyPart(BodyPart bodyPart)
        {
            databaseManager.Connection.Update(bodyPart);
        }

        // Save ChassisPart to the database.
        public void SaveChassisPart(string name, string prefabPath, int cost, int wheelCount)
        {
            var chassisPart = new ChassisPart()
            {
                Name = name,
                PrefabPath = prefabPath,
                Cost = cost,
                WheelCount = wheelCount,
                Valid = 1
            };
            databaseManager.Connection.Insert(chassisPart);
        }

        // Save ChassisPart to the database.
        public void SaveChassisPart(ChassisPart chassisPart)
        {
            databaseManager.Connection.Insert(chassisPart);
        }

        // Update ChassisPart in the database.
        public void UpdateChassisPart(int id, string name, string prefabPath, int cost, int wheelCount)
        {
            var chassisPart = GetChassisPartById(id);
            chassisPart.Name = name;
            chassisPart.PrefabPath = prefabPath;
            chassisPart.Cost = cost;
            chassisPart.WheelCount = wheelCount;
            databaseManager.Connection.Update(chassisPart);
        }

        // Update ChassisPart in the database.
        public void UpdateChassisPart(ChassisPart chassisPart)
        {
            databaseManager.Connection.Update(chassisPart);
        }

        // Save Loadout to the database.
        public void SaveLoadout(int playerId, string name, int bodyPartId, int wheelPartId, int chassisPartId)
        {
            var loadout = new Loadout();
            loadout.BodyPartID = bodyPartId;
            loadout.WheelPartID = wheelPartId;
            loadout.ChassisPartID = chassisPartId;
            loadout.PlayerID = playerId;
            loadout.Name = name;
            loadout.Valid = 1;
            databaseManager.Connection.Insert(loadout);
        }

        // Save Loadout to the database.
        public void SaveLoadout(Loadout loadout)
        {
            databaseManager.Connection.Insert(loadout);
        }

        // Update Loadout in the database.
        public void UpdateLoadout(int id, int playerId, string name, int bodyPartId, int wheelPartId, int chassisPartId)
        {
            var loadout = GetLoadoutById(id);
            loadout.BodyPartID = bodyPartId;
            loadout.WheelPartID = wheelPartId;
            loadout.ChassisPartID = chassisPartId;
            loadout.PlayerID = playerId;
            loadout.Name = name;
            databaseManager.Connection.Update(loadout);
        }

        // Update Loadout in the database.
        public void UpdateLoadout(Loadout loadout)
        {
            databaseManager.Connection.Update(loadout);
        }

        // Invalidate WheelPart
        public void InvalidateWheelPart(string name)
        {
            var wheelPart = GetWheelPartByName(name);
            wheelPart.Valid = 0;
            databaseManager.Connection.Update(wheelPart);
        }

        // Invalidate WheelPart
        public void InvalidateWheelPart(int id)
        {
            var wheelPart = GetWheelPartById(id);
            wheelPart.Valid = 0;
            databaseManager.Connection.Update(wheelPart);
        }

        // Invalidate WheelPart
        public void InvalidateWheelPart(WheelPart wheelPart)
        {
            wheelPart.Valid = 0;
            databaseManager.Connection.Update(wheelPart);
        }

        // Invalidate BodyPart
        public void InvalidateBodyPart(string name)
        {
            var bodyPart = GetBodyPartByName(name);
            bodyPart.Valid = 0;
            databaseManager.Connection.Update(bodyPart);
        }

        // Invalidate BodyPart
        public void InvalidateBodyPart(int id)
        {
            var bodyPart = GetBodyPartById(id);
            bodyPart.Valid = 0;
            databaseManager.Connection.Update(bodyPart);
        }

        // Invalidate BodyPart
        public void InvalidateBodyPart(BodyPart bodyPart)
        {
            bodyPart.Valid = 0;
            databaseManager.Connection.Update(bodyPart);
        }

        // Invalidate ChassisPart
        public void InvalidateChassisPart(string name)
        {
            var chassisPart = GetChassisPartByName(name);
            chassisPart.Valid = 0;
            databaseManager.Connection.Update(chassisPart);
        }

        // Invalidate ChassisPart
        public void InvalidateChassisPart(int id)
        {
            var chassisPart = GetChassisPartById(id);
            chassisPart.Valid = 0;
            databaseManager.Connection.Update(chassisPart);
        }

        // Invalidate ChassisPart
        public void InvalidateChassisPart(ChassisPart chassisPart)
        {
            chassisPart.Valid = 0;
            databaseManager.Connection.Update(chassisPart);
        }

        // Invalidate Loadout
        public void InvalidateLoadout(Loadout loadout)
        {
            loadout.Valid = 0;
            databaseManager.Connection.Update(loadout);
        }

        // Invalidate Loadout
        public void InvalidateLoadout(int id)
        {
            var loadout = GetLoadoutById(id);
            loadout.Valid = 0;
            databaseManager.Connection.Update(loadout);
        }

        // When Player buys WheelPart save it so it will be his forever.
        public void AssignWheelPartToPlayer(int playerID, int wheelPartID)
        {
            var playerWheelPart = new PlayerWheelPart(playerID, wheelPartID);

            databaseManager.Connection.Insert(playerWheelPart);
        }

        // When Player buys WheelPart save it so it will be his forever.
        public void AssignWheelPartToPlayer(string username, int wheelPartID)
        {
            var player = GetPlayerByUsername(username);

            var playerWheelPart = new PlayerWheelPart(player.PlayerID, wheelPartID);

            databaseManager.Connection.Insert(playerWheelPart);
        }

        // When Player buys BodyPart save it so it will be his forever.
        public void AssignBodyPartToPlayer(int playerID, int bodyPartID)
        {
            var playerBodyPart = new PlayerBodyPart(playerID, bodyPartID);

            databaseManager.Connection.Insert(playerBodyPart);
        }

        // When Player buys BodyPart save it so it will be his forever.
        public void AssignBodyPartToPlayer(string username, int bodyPartID)
        {
            var player = GetPlayerByUsername(username);

            var playerBodyPart = new PlayerBodyPart(player.PlayerID, bodyPartID);

            databaseManager.Connection.Insert(playerBodyPart);
        }

        // When Player buys ChassisPart save it so it will be his forever.
        public void AssignChassisPartToPlayer(int playerID, int chassisPartID)
        {
            var playerChassisPart = new PlayerChassisPart(playerID, chassisPartID);

            databaseManager.Connection.Insert(playerChassisPart);
        }

        // When Player buys ChassisPart save it so it will be his forever.
        public void AssignChassisPartToPlayer(string username, int chassisPartID)
        {
            var player = GetPlayerByUsername(username);

            var playerChassisPart = new PlayerChassisPart(player.PlayerID, chassisPartID);

            databaseManager.Connection.Insert(playerChassisPart);
        }

        // Alex - Called when new player is created
        public void AssignDefaultsToPlayer(int playerID)
        {
            var chassis = GetAllChassisParts();
            if(chassis.Count > 0 && chassis[0] != null && chassis[0].ChassisPartID == 1)
                AssignChassisPartToPlayer(playerID, 1);

            var body = GetAllBodyParts();
            if(body.Count > 0 && body[0] != null && body[0].BodyPartID == 1)
                AssignBodyPartToPlayer(playerID, 1);

            var wheels = GetAllWheelParts();
            if(wheels.Count > 0 && wheels[0] != null && wheels[0].WheelPartID == 1)
                AssignWheelPartToPlayer(playerID, 1);
        }

        // Alex - Called to check if player has defaults set
        public bool PlayerHasDefaults(int playerID)
        {
            var chassis = GetAllChassisPartsOwnedByPlayer(playerID);
            if (chassis.Count <= 0 || chassis[0] == null || chassis[0].ChassisPartID != 1)
                return false;

            var body = GetAllBodyPartsOwnedByPlayer(playerID);
            if (body.Count <= 0 || body[0] == null || body[0].BodyPartID != 1)
                return false;

            var wheels = GetAllWheelPartsOwnedByPlayer(playerID);
            if (wheels.Count <= 0 || wheels[0] == null || wheels[0].WheelPartID != 1)
                return false;

            return true;
        }


        // Return list of all WheelParts owned by a Player with given PlayerID.
        public IList<WheelPart> GetAllWheelPartsOwnedByPlayer(int playerId)
        {
            var wheelPartsIds = databaseManager.Connection.Table<PlayerWheelPart>().Where(wp => wp.PlayerID == playerId).ToList().Select(pwp => pwp.WheelPartID).ToList();
            var wheelParts = GetWheelPartsByIds(wheelPartsIds);
            return wheelParts;
        }

        // Return list of all WheelParts owned by a Player with given Username.
        public IList<WheelPart> GetAllWheelPartsOwnedByPlayer(string username)
        {
            var player = GetPlayerByUsername(username);
            var wheelPartsIds = databaseManager.Connection.Table<PlayerWheelPart>().Where(wp => wp.PlayerID == player.PlayerID).ToList().Select(pwp => pwp.WheelPartID).ToList();
            var wheelParts = GetWheelPartsByIds(wheelPartsIds);
            return wheelParts;
        }

        // Return list of all BodyParts owned by a Player with given PlayerID.
        public IList<BodyPart> GetAllBodyPartsOwnedByPlayer(int playerId)
        {
            var bodyPartsIds = databaseManager.Connection.Table<PlayerBodyPart>().Where(bp => bp.PlayerID == playerId).ToList().Select(pbp => pbp.BodyPartID).ToList();
            var bodyParts = GetBodyPartsByIds(bodyPartsIds);
            return bodyParts;
        }

        // Return list of all BodyParts owned by a Player with given Username.
        public IList<BodyPart> GetAllBodyPartsOwnedByPlayer(string username)
        {
            var player = GetPlayerByUsername(username);
            var bodyPartsIds = databaseManager.Connection.Table<PlayerBodyPart>().Where(bp => bp.PlayerID == player.PlayerID).ToList().Select(pbp => pbp.BodyPartID).ToList();
            var bodyParts = GetBodyPartsByIds(bodyPartsIds);
            return bodyParts;
        }

        // Return list of all ChassisParts owned by a Player with given PlayerID.
        public IList<ChassisPart> GetAllChassisPartsOwnedByPlayer(int playerId)
        {
            var chassisPartsIds = databaseManager.Connection.Table<PlayerChassisPart>().Where(cp => cp.PlayerID == playerId).ToList().Select(pcp => pcp.ChassisPartID).ToList();
            var chassisParts = GetChassisPartsByIds(chassisPartsIds);
            return chassisParts;
        }

        // Return list of all ChassisParts owned by a Player with given Username.
        public IList<ChassisPart> GetAllChassisPartsOwnedByPlayer(string username)
        {
            var player = GetPlayerByUsername(username);
            var chassisPartsIds = databaseManager.Connection.Table<PlayerChassisPart>().Where(cp => cp.PlayerID == player.PlayerID).ToList().Select(pcp => pcp.ChassisPartID).ToList();
            var chassisParts = GetChassisPartsByIds(chassisPartsIds);
            return chassisParts;
        }

        // Function used to clear and reseed the incrementation value of given table name
        public void ClearTable(string tableName)
        {
            databaseManager.Connection.Execute(
                string.Format("DELETE FROM {0}; DELETE FROM SQLITE_SEQUENCE WHERE NAME = '{0}'", tableName));
        }

        private ObjectModel.Player GetPlayerByUsername(string username)
        {
            var user = databaseManager.Connection.Table<User>().FirstOrDefault(u => u.Username.Equals(username) && u.Valid == 1);
            if (user == null)
            {
                return null;
            }

            var player = databaseManager.Connection.Table<ObjectModel.Player>().FirstOrDefault(p => p.UserID == user.UserID && p.Valid == 1);
            if (player == null)
            {
                return null;
            }

            return player;
        }

    }
}