﻿// Author: Alex Kitching
// Scriptable Object Class Responsible for Biome Functionality
using UnityEngine;

namespace Assets.Scripts.ProceduralScenery
{
    [CreateAssetMenu]
    public sealed class Biome : ScriptableObject
    {
        [SerializeField]
        private SceneryAsset[] _assets;

        private float _totalFreq = 0f;

        [SerializeField]
        private int _maxLength = 0;
        [SerializeField]
        private int _minLength = 0;

        public Biome[] BiomesToTransition;

        public void Initialise()
        {
            _totalFreq = 0f;
            // Calculate Total Frequency of Assets
            GetTotalAssetFreq();
        }

        private void GetTotalAssetFreq()
        {
            // For Each asset, add frequency to Total Frequency
            for (int i = 0; i < _assets.Length; i++)
            {
                _totalFreq += _assets[i].Frequency;
            }
        }

        // Generates Length of biome
        public int GenerateLength()
        {
            return Mathf.RoundToInt(Random.Range(_minLength, _maxLength));
        }

        public GameObject GetSceneryObject()
        {
            // Returns Gameobject via Weighted Random Generation
            return _assets[GetWeightedRandom()].Object;
        }

        // Gets weighted Random number based on Biome Asset Weights
        private int GetWeightedRandom()
        {
            // Rolls Number between 0 and Total Frequency
            float roll = Random.Range(0, _totalFreq);

            // Index to Return
            int index = -1;

            // For each Asset
            for (int i = 0; i < _assets.Length; i++)
            {
                if (_assets[i].IsOneShot && _assets[i].OneShotFired)
                    continue;

                // Roll is Less than or Equal to Assets Freq we have a hit
                if(roll <= _assets[i].Frequency)
                {
                    index = i;
                    break;
                }

                // Subtract Assets Freq from Roll
                roll -= _assets[i].Frequency;

                if (i != _assets.Length - 1) continue;

                i = 0;
                roll = Random.Range(0, _totalFreq);
            }

            if (_assets[index].IsOneShot)
            {
                _assets[index].OneShotFired = true;
            }

            return index;
        }
    }
}
