﻿// Author: Alex Kitching
// Monobehaviour Class Responsible for holding left and right scenery pivot points for use with spawning scenery assets
using UnityEngine;

namespace Assets.Scripts.ProceduralScenery
{
    public sealed class Section : MonoBehaviour
    {
        public Transform leftSceneryHolder;
        public Transform rightSceneryHolder;
    }
}
