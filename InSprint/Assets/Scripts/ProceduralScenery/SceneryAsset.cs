﻿// Author: Alex Kitching
// Scriptable Object Class Responsible biome Scenery Assets
using UnityEngine;

namespace Assets.Scripts.ProceduralScenery
{
    [CreateAssetMenu]
    public sealed class SceneryAsset : ScriptableObject
    {
        // Prefab
        public GameObject Object;
        // Frequency
        public float Frequency = 0f;

        // Whether Asset should be spawned only once during the biome
        public bool IsOneShot = false;

        public SceneryAsset()
        {
            OneShotFired = false;
        }

        // Whether asset has been spawned in the current biome
        public bool OneShotFired { get; set; }
    }
}
