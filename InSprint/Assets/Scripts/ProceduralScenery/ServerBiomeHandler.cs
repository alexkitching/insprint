﻿// Author: Alex Kitching
// Class Responsible biome Scenery Assets
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.ProceduralScenery
{
    public sealed class ServerBiomeHandler : NetworkBehaviour
    {
        // Current biome Index (Synced between Clients)
        [SyncVar]
        private int _currentBiomeIndex;

        [SerializeField]
        private Biome _startingBiome;

        public Biome CurrentBiome { get; private set; }

        [SyncVar (hook = "OnLengthReduced")]
        private int _remainingLength;

        // Sets up Start Biome
        public void SetupStartBiome()
        {
            CurrentBiome = _startingBiome;
            CurrentBiome.Initialise();

            _remainingLength = CurrentBiome.GenerateLength();
        }

        // Reduces Current Biomes Remaining Length
        public void ReduceBiomeLengthByOneUnit()
        {
            --_remainingLength;
        }

        // Called on Biome Length Changed
        private void OnLengthReduced(int a_length)
        {
            if (a_length != 0) return;
            // Biome Length now 0, Biome is changed on clients
            ChangeServerBiome();
            RpcOnBiomeIndexChanged(_currentBiomeIndex);
        }

        // Changes Current Biome Index
        private void ChangeServerBiome()
        {
            _currentBiomeIndex = CurrentBiome.BiomesToTransition.Length == 1 ? 0 : Random.Range(0, CurrentBiome.BiomesToTransition.Length);
        }

        // Called on all Clients to update Current Biome based on new index
        [ClientRpc]
        private void RpcOnBiomeIndexChanged(int a_index)
        {
            CurrentBiome = CurrentBiome.BiomesToTransition[a_index];
            CurrentBiome.Initialise();

            _remainingLength = CurrentBiome.GenerateLength();
        }
    }
}
